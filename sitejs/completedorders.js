//list completed orders starts here
userdetail = [];
role_id = null;

function listcompletedorders(type) {
    sessionStorage.order_type = 3;
    sessionStorage.order_page = type;
    userdetail = JSON.parse(localStorage.userdetails);
    role_id = userdetail['role']['id'];
    
    if (role_id != 2 || $("#search_query").val() != "") {
        if (type == 0) {
            var url = listorder_api;
        } else {
            var url = type;
        }


        var postData = JSON.stringify({
            "status": 4,
            "start_date": $("#date_picker").val(),
            "end_date": $("#date_picker1").val(),
            "search": $("#search_query").val()

        });
        $.ajax({
            url: url,
            type: 'POST',
            data: postData,
            headers: {
                "content-type": 'application/json',
                "Authorization": "Token " + localStorage.token
            },
            success: function(data) {
                $(".transaction_amt").text("Rs. " + (data.transactions).toFixed(2));
                $(".nocompleteddata").remove();
                $(".findalldr").hide();
                $(".gobtnall").attr("disabled", false);
                if (data.results.length == 0) {
                    $(".completeddiv").hide();
                    $(".completedorders").prepend('<center class="nocompleteddata"><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>');


                } else {
                    $(".completeddiv").show();
                    $(".completeddynload").empty();

                    for (var i = 0; i < data.results.length; i++) {
                        if (data.results[i].pickup_image == null || data.results[i].pickup_image.length == 0) {
                            var image = '../img/noimage.png';
                        } else {
                            var image = data.results[i].pickup_image[0].image;
                        }
                        if (data.results[i].delivery_image == null || data.results[i].delivery_image.length == 0) {
                            var image1 = '../img/noimage.png';
                        } else {
                            var image1 = data.results[i].delivery_image[0].image;
                        }
                        var created_on = data.results[i].created_on;
                        var newdate = created_on.split('T');
                        var now = newdate[0].split('-');
                        var timeval = Convert_time(newdate[1].slice(0, 8));
                        var datenew = now[2] + '/' + now[1] + '/' + now[0];

                        var picktype = (data.results[i].pickup_type) ? '../img/pick.svg' : '../img/buy.svg';
                        // {/* <td onclick="viewproduct(this,${data.results[i].id},0)"><center><img src="${image}" class="img-responsive tab_img" href="#viewfullimg" data-toggle="modal"></center></td><td>${data.results[i].product}</td> */}

                        $(".completeddynload").append(`<tr><td>${(i + 1)}</td><td>${data.results[i].order_id}</td><td>${data.results[i].pickup_details[0].name}</td><td>${datenew} ${timeval}</td><td><img src=${picktype} class="width50"></td><td onclick="viewproduct(this,${data.results[i].id},0)"><center><img src="${image}" class="img-responsive classborderedimg  tab_img" href="#viewfullimg" data-toggle="modal"></center></td><td onclick="viewproduct(this,${data.results[i].id},1)"><center><img src="${image1}" class="img-responsive classborderedimg  tab_img" href="#viewfullimgdelvery" data-toggle="modal"></center></td><td><a onclick="appendlocationcomp(${data.results[i].id});" class="asntsk">More</a></td></tr>`);
                    }
                }
                if ((data.next_url != null) || (data.prev_url != null)) {
                    $("#urlloader").show();

                    var next1 = (data.next_url).split('=');
                    var val = data.count / data.page_size;
                    if (val % 1 === 0) {
                        //if number is integer
                        var val = val;
                    } else {
                        var val = parseInt(val) + 1;
                    }
                    var obj = $('#pagination').twbsPagination({
                        totalPages: val,
                        visiblePages: 5,
                        onPageClick: function(event, page) {
                            console.info(page);
                            listcompletedorders(next1[0] + "=" + (page));
                        }
                    });
                } else {
                    $("#urlloader").hide();
                }
            },
            error: function(data) {
                $(".findalldr").hide();
                $(".gobtnall").attr("disabled", false);
                // $(".completedclass").hide();
                for (var key in JSON.parse(data.responseText)) {
                    $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
                }
                showerrtoast();
            }
        });
    }
}

//append data
function appendlocationcomp(me) {
    localStorage.userid = me

    $.ajax({
        url: singleorder_api + me + '/',
        type: 'get',
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.token
        },
        success: function(data) {
            $(".orderid_compltd").text(data.order_id);
            $('.corner , .corner1,.corner2').css("border-bottom", "4px solid red");
            $('.completedcost').text(" " + data.orderdetails.delivery_cost);
            var picktype = (data.pickup_type) ? 'Pick & Deliver' : 'Buy & Deliver';
            $('.completepicktype').text(" " + picktype);
            $('.completetype').text(" " + data.payment_type);
            $('#appendpickupdetcomp .pickernamecomp').text(" " + data.pickup_name);
            $('#appendpickupdetcomp .pickerph_nocomp').text(" " + data.pickup_mob);
            $('#appendpickupdetcomp .pickerloccomp').text(" " + data.pickup_loc);
            $('#appenddeldetcomp .deliverynamecomp').text(" " + data.del_name);
            $('#appenddeldetcomp .deliveryph_nocomp').text(" " + data.del_mob);
            $('#appenddeldetcomp .deliveryloccomp').text(" " + data.del_loc);
            $('#appendtimecompld .distcomp').text(" " + data.orderdetails.approx_dist + " Kms");
            $('#appendtimecompld .estimatecomp').text(time_is);
            var cost = (data.orderdetails.product_cost == null) ? 0 : data.orderdetails.product_cost;
            $('#appendtimecompld .paymtdetcomp').text(" Rs." + (parseFloat(data.orderdetails.delivery_cost) + parseFloat(cost)));
            // $('.deliveryboynamecomp').text(" " +data.order_assignment[0].assign_employee.first_name);
            // $('.deliveryboyph_nocomp').text(" " +data.order_assignment[0].assign_employee.username);
            // $('.editclassemp').attr('src', data.order_assignment[0].assign_employee.profile_pic);cancel_reason
            $(".listemp_details").empty();
            for (var i = 0; i < data.order_assignment.length; i++) {
                var datecreate = data.order_assignment[i].created_on.split('T')
                var content = datecreate[0].split('-');
                var created_date = content[2] + " " + month[content[1] - 1] + " " + content[0];
                $(".listemp_details").append(`  <tr>
            <td> ${data.order_assignment[i].assign_employee.first_name} </td>
            <td> ${data.order_assignment[i].assign_employee.username}  </td>
            <td> ${(data.order_assignment[i].assign_employee.cancel_reason == null)? "Order Delivered" : data.order_assignment[i].assign_employee.cancel_reason}  </td>
            <td> ${created_date} </td>
            <td> ${data.order_assignment[i].employee.first_name} </td>
          </tr>`);
            }

            var time_taken = (data.orderdetails.time_taken).split(":");
            if (time_taken[0] != "00") {
                var time_is = data.orderdetails.time_taken;
                $('.timeexp').text(time_is + " Hours");
            } else if (time_taken[1] != "00") {
                var time_is = data.orderdetails.time_taken.slice(3);
                $('.timeexp').text(time_is + " Mins");
            } else {
                $('.timeexp').text('0 Min');
            }

            $(".product_detailscomp").empty();
            var units = ["None", "Kg", "Ltr", "Qty"];

            ((data.descriptions == null) || (data.descriptions == "")) ? $(".description_note").text("Not Mentioned"): $(".description_note").text(data.descriptions);

            for (var i = 0; i < data.categories.length; i++) {
                $(".product_detailscomp").append(`<b class="aprxdet">${data.categories[i].category.name}</b> <span class="appdcategory${i}"></span><br>`);
                for (var j = 0; j < data.categories[i].products.length; j++) {
                    if (j != (data.categories[i].products.length - 1)) {
                        $(".appdcategory" + i).append(`${ data.categories[i].products[j].name +"-" +data.categories[i].products[j].quantity + " " + units[parseInt(data.categories[i].products[j].units) - 1] },`);
                    } else {
                        $(".appdcategory" + i).append(`${ data.categories[i].products[j].name +"-" +data.categories[i].products[j].quantity + " " + units[parseInt(data.categories[i].products[j].units) - 1] }`);
                    }
                }
            }
            $("#modal_ordercmp").modal();
            listcompletedorders(0);
        },
        error: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}