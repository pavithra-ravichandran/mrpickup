var domain = "http://testapi.zordec.com/";
// var domain = "http://mrpickup.zordec.com/";

//auth fn starts here
var login_api = domain + 'oauth/login/';//done
var logout_api = domain + 'oauth/logout/';
var forgotpwd_api = domain + "auth/forgot-password/";
var resetpassword_api = domain + 'auth/reset-password/';
var changepasswordforadmin_api = domain + 'oauth/change-password/';

//changed api
var listemployees_api = domain + 'employee/list_user/';//done
var listemployeesrole_api = domain + 'employee/list/category/'; //done
var addemployees_api = domain + 'employee/';//done
var addorder_api = domain + 'orders/';
var listorder_api = domain  + 'orders/list_orders/';

//notification api
var notification_api = domain + "orders/notification/";

//load for customer history page
var customerhistory_api = domain + 'customers/';

var singleorder_api = domain + 'orders/';

// list category
var listcategory_api = domain + "rates/product/";
//rate
var listrate_api = domain + "rates/";
//OFFERS
var offers_api = domain + "rates/offers/";
//list product image
var listproductimg_api = domain + 'orders/';
//assign employee
var listemployeenearest_api = domain + 'employee/delivery_boy/';
//assign employee
var assignemployee_api = domain + 'employee/delivery_boy/';
//prefill user address api
var prefill_usraddr_api = domain + "customers/user_address/";

//list units application
var unitsapi = domain + "orders/unit/";

// rate clearance
var rateclearance_api = domain + "accounts/";


//api for edit employees
//api to be changed
// var dashboardcountvalue_api = domain + 'admin/dashboard/';
var dashboardcountvalue_api = domain + 'customers/dashboard/';


// unwanted api

var addsubadmin_api = domain + 'admin/list/create/sub-admin/';
var listsubadmin_api = domain + 'admin/list/create/sub-admin/';
// var passwordset_foremployee_api = domain + '';
var deletesubadmin_api = domain + 'admin/delete/sub-admin/';
var listcompletedorders_api = domain + 'admin/list/order/?status=5'; //1=placed,234=pending,5=completed    - url starts here *orders/list_orders/
var listplacedorders_api = domain + 'admin/list/order/?status=1';
var listpendingorders_api = domain + 'admin/list/order/?status=2';
var listcancelledorders_api = domain + 'admin/list/order/?status=6';
var listrequestdenied_api = domain + 'admin/list/order/?status=8';

// var assigncollectionman_api = domain + 'admin/assign/collection/man/';
var bookingstatus_api = domain + 'admin/list/booking/status/';
var bookingstatusupdate_api = domain + 'admin/update/book/status/';
// var assigncollectionmanbd_api = domain + 'admin/assign/collection/man/';

// 'admin/list/employee/';
var changepwdsubadmin_api = domain + 'admin/update/sub-admin/';
// var deleteemploye_api = domain + 'admin/delete/employee/';
var searchbyname_api = domain + "admin/list/employee/?search=";
