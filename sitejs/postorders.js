$(".show_store").hide();
var prodimg = [],
    addprod_images = [],
    or_rate = ur_rate = "";
$("#pickuppn").focusout(function () {
    $('#deliverypn').val($("#pickuppn").val());
});
loadunits();
loadrate();
var categories = '',
    quantities = '';

function loadcategory() {
    $.ajax({
        url: listemployeesrole_api,
        type: 'get',
        headers: {
            "content-type": 'application/json'
            // "Authorization": "Token " + localStorage.token
        },
        success: function (data) {
            // $('.addcatgyreplica').empty();
            for (var i = 0; i < data.category.length; i++) {
                // $('#addcatgy').append('<option value=' + data.category[i].id + '>' + data.category[i].name + '</option>');
                $('.addcatgyreplica').append('<option value=' + data.category[i].id + '>' + data.category[i].name + '</option>');
                categories += '<option value=' + data.category[i].id + '>' + data.category[i].name + '</option>';
            }
        },

        error: function (data) {
            console.log("error occured during category load");
        }
    });
}

function loadrate() {
    $.ajax({
        url: listrate_api,
        type: 'get',
        headers: {
            "content-type": 'application/json'
            // "Authorization": "Token " + localStorage.token
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].delivery_type == "OR") {
                    or_rate += '<option value=' + data[i].id + ' data-type=' + data[i].delivery_type + '>' + data[i].from_weight + " Kg-" + data[i].to_weight + ' Kg</option>';
                } else {
                    ur_rate += '<option value=' + data[i].id + ' data-type=' + data[i].delivery_type + '>' + data[i].from_weight + " Kg-" + data[i].to_weight + ' Kg</option>';
                }
            }
        },

        error: function (data) {
            console.log("error occured during category load");
        }
    });
}

$("#deliverytype").change(function () {
    $("#weightrange").empty();
    if ($("#deliverytype").val() == "OR") {
        $("#weightrange").append(or_rate);
    } else {
        $("#weightrange").append(ur_rate);
    }
})

function loadunits() {
    $.ajax({
        url: unitsapi,
        type: "GET",
        headers: {
            "content-type": 'application/json'
            // "Authorization": "Token " + localStorage.token
        },
        success: function (data) {
            $(".quantity").empty();
            for (var i = 0; i < data.length; i++) {
                // $('#addcatgy').append('<option value=' + data.category[i].id + '>' + data.category[i].name + '</option>');
                $(".quantity").append(`${(i == 0) ? `<label class="btn active pl0">
                                            <input value="${data[i].id}" data-value="${data[i].name}" type="radio" name='${data[i].name}' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-dot-circle-o fa-2x"></i> <span>  ${data[i].name}</span>
                                        </label>` : `<label class="btn pl0">
                                            <input type="radio" data-value="${data[i].name}" value="${data[i].id}" name='${data[i].name}'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-dot-circle-o fa-2x"></i> <span>  ${data[i].name}</span>
                                        </label>`} `)
                // $('.quantity').append('<option value=' + data[i].id + '>' + data[i].name + '</option>');
                quantities += (i == 0) ? `<label class="btn active pl0">
                                            <input value="${data[i].id}" data-value="${data[i].name}" type="radio" name='${data[i].name}' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-dot-circle-o fa-2x"></i> <span>  ${data[i].name}</span>
                                        </label>` : `<label class="btn pl0">
                                            <input type="radio" value="${data[i].id}" data-value="${data[i].name}" name='${data[i].name}'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-dot-circle-o fa-2x"></i> <span>  ${data[i].name}</span>
                                        </label>`;
            }

            $(".quantity .btn.pl0").click(function () {
                $(".quantity input").attr("checked", false);
                $(this).find("input").attr("checked", true);

            });
        },
        error: function (data) {
            console.log("error occured during category load");
        }
    })
}
$(function () {

    if (window.File && window.FileList && window.FileReader) {
        // addproduct images change fn
        $("#addimages").on("change", function (e) {
            $(".addfileloctn_prod_images").empty();
            addprod_images = [];
            var files = e.target.files;
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                addprod_images.push(file);
                add_img_Files(file, i);
            }
        });
    } //add image ends here
    loadcategory();
})
//image file on load
//ext local files append fn
function add_img_Files(f, i) {
    var reader = new FileReader();
    reader.onload = (function (file) {
        return function (e) {
            $(".uploadprodimage").append("<div class='col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mt10 plr5 addimage" + file.lastModified + "'> <div class='itemsContainer'> <div class='image image_mt10'> <img src='" + e.target.result + "' class='img-responsive uploadimg' title='" + file.name + "' style='height: 100px;width: 100px;'/> </div><div class='play'> <a onclick='deletethisimage_local(" + file.lastModified + ",\"" + file.name + "\")' class='bt_pointer'><img src='../img/deletebtn.svg'/></a> </div></div></div>");

        };
    })(f, i);
    reader.readAsDataURL(f);
}
//delete image starts hre
//delete images locally fn starts here
function deletethisimage_local(i, fileName) {
    $('.addimage' + i).remove();
    for (var i = 0; i < addprod_images.length; i++) {
        if (addprod_images[i].name === fileName) {
            addprod_images.splice(i, 1);
            break;
        }
    }
}

function submitaddorders() { //submit functionality starts here
    var postData = new FormData();
    var delivery = [];
    var pickup = [];

    // new validation starts here
    if ($("#order_type_val").find("input:checked").data("value") == 0) {
        if ($(".pickup-name-len").eq(0).val() == "") {
            $("#snackbarerror").text("Pickup name is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".pickup-name-len").eq(0).parent().parent().next().find("input").val() == "") {
            $("#snackbarerror").text("Pickup number is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".pickup-name-len").eq(0).parent().parent().parent().next().find("input").val() == "") {
            $("#snackbarerror").text("Pickup address is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }

        if ($(".delivery-name-len").eq(0).val() == "") {
            $("#snackbarerror").text("Delivery name is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".delivery-name-len").eq(0).parent().parent().next().find("input").val() == "") {
            $("#snackbarerror").text("Delivery number is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".delivery-name-len").eq(0).parent().parent().parent().next().find("input").val() == "") {
            $("#snackbarerror").text("Delivery address is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }

        var products = [], pickup0_details = [], delivery0_details = [], product_name = [];
        for (var i = 0; i < $(".replicatediv").length; i++) {
            for (j = 0; j < $(".replicatediv").eq(i).find(".subreplicate").length; j++) {
                if (($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val() != "") && ($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".quantity .active input").val() != "") && ($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_count").val() != "")) {
                    product_name.push({
                        "name": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val(),
                        "category_id": $(".replicatediv").eq(i).find("select").val(),
                        "units_id": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".quantity .active input").val(),
                        "quantity": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_count").val()
                    });
                    products.push({
                        "category": $(".replicatediv").eq(i).find("select").val(),
                        "name": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val()
                    })
                } else {
                    $("#snackbarerror").text("Enter the Product details to add another address");
                    showiperrtoast();
                    event.stopPropagation();
                    return;
                }
            }
        }
        pickup0_details = [{
            "name": $(".pickup-name-len").eq(0).val(),
            "mobile": $(".pickup-name-len").eq(0).parent().parent().next().find("input").val(),
            "locations": $(".pickup-name-len").eq(0).parent().parent().parent().find("input").eq(2).val(),
            "products": products
        }]
        delivery0_details = [{
            "name": $(".delivery-name-len").eq(0).val(),
            "mobile": $(".delivery-name-len").eq(0).parent().parent().next().find("input").val(),
            "locations": $(".delivery-name-len").eq(0).parent().parent().parent().find("input").eq(2).val(),
            "products": products
        }]
        postData.append("pickup_details", JSON.stringify(pickup0_details));
        postData.append("delivery_details", JSON.stringify(delivery0_details));



    } else if ($("#order_type_val").find("input:checked").data("value") == 1) {
        if ($(".delivery-name-len").eq(0).val() == "") {
            $("#snackbarerror").text("Delivery name is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".delivery-name-len").eq(0).parent().parent().next().find("input").val() == "") {
            $("#snackbarerror").text("Delivery number is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".delivery-name-len").eq(0).parent().parent().parent().find("input").eq(2).val() == "") {
            $("#snackbarerror").text("Delivery address is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".pickup-name-len").eq(0).val() == "") {
            $("#snackbarerror").text("Pickup name is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".pickup-name-len").eq(0).parent().parent().next().find("input").val() == "") {
            $("#snackbarerror").text("Pickup number is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".pickup-name-len").eq(0).parent().parent().parent().find("input").eq(2).val() == "") {
            $("#snackbarerror").text("Pickup address is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }


        var products = [], product_name = [], personal_details_pickup = [];
        for (var i = 0; i < $(".replicatediv").length; i++) {
            for (j = 0; j < $(".replicatediv").eq(i).find(".subreplicate").length; j++) {
                if (($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val() != "") && ($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".quantity .active input").val() != "") && ($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_count").val() != "")) {
                    product_name.push({
                        "name": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val(),
                        "category_id": $(".replicatediv").eq(i).find("select").val(),
                        "units_id": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".quantity .active input").val(),
                        "quantity": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_count").val()
                    });
                    products.push({
                        "category": $(".replicatediv").eq(i).find("select").val(),
                        "name": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val()
                    })
                } else {
                    $("#snackbarerror").text("Enter the Product details to add another address");
                    showiperrtoast();
                    event.stopPropagation();
                    return;
                }
            }
        }
        product_list.push(products);

        personal_details_pickup = {
            "name": $(".pickup-name-len").eq(0).val(),
            "mobile": $(".pickup-name-len").eq(0).parent().parent().next().find("input").val(),
            "locations": $(".pickup-name-len").eq(0).parent().parent().parent().find("input").eq(2).val(),
            "products": product_list[0]
        };
        product_name_push.push(product_name);
        address_details.push(personal_details_pickup);
        var delivery1_details;
        delivery1_details = [{
            "name": $(".delivery-name-len").eq(0).val(),
            "mobile": $(".delivery-name-len").eq(0).parent().parent().next().find("input").val(),
            "locations": $(".delivery-name-len").eq(0).parent().parent().parent().find("input").eq(2).val(),
            "products": product_list[0]
        }]
        postData.append("pickup_details", JSON.stringify(address_details));
        postData.append("delivery_details", JSON.stringify(delivery1_details));


    } else if ($("#order_type_val").find("input:checked").data("value") == 2) {
        if ($(".pickup-name-len").eq(0).val() == "") {
            $("#snackbarerror").text("Pickup name is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".pickup-name-len").eq(0).parent().parent().next().find("input").val() == "") {
            $("#snackbarerror").text("Pickup number is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".pickup-name-len").eq(0).parent().parent().parent().next().find("input").val() == "") {
            $("#snackbarerror").text("Pickup address is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".delivery-name-len").eq(0).val() == "") {
            $("#snackbarerror").text("Delivery name is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".delivery-name-len").eq(0).parent().parent().next().find("input").val() == "") {
            $("#snackbarerror").text("Delivery number is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".delivery-name-len").eq(0).parent().parent().parent().find("input").eq(2).val() == "") {
            $("#snackbarerror").text("Delivery address is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        var products = [], product_name = [], personal_details_delup = [];
        for (var i = 0; i < $(".replicatediv").length; i++) {
            for (j = 0; j < $(".replicatediv").eq(i).find(".subreplicate").length; j++) {
                if (($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val() != "") && ($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".quantity .active input").val() != "") && ($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_count").val() != "")) {
                    product_name.push({
                        "name": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val(),
                        "category_id": $(".replicatediv").eq(i).find("select").val(),
                        "units_id": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".quantity .active input").val(),
                        "quantity": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_count").val()
                    });
                    products.push({
                        "category": $(".replicatediv").eq(i).find("select").val(),
                        "name": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val()
                    })
                } else {
                    $("#snackbarerror").text("Enter the Product details to add another address");
                    showiperrtoast();
                    event.stopPropagation();
                    return;
                }
            }
        }

        product_list.push(products);

        personal_details_delup = {
            "name": $(".delivery-name-len").eq(0).val(),
            "mobile": $(".delivery-name-len").eq(0).parent().parent().next().find("input").val(),
            "locations": $(".delivery-name-len").eq(0).parent().parent().parent().find("input").eq(2).val(),
            "products": product_list[0]
        };
        product_name_push.push(product_name);
        address_details.push(personal_details_delup);
        var pickup2_details = [];
        pickup2_details = [{
            "name": $(".pickup-name-len").eq(0).val(),
            "mobile": $(".pickup-name-len").eq(0).parent().parent().next().find("input").val(),
            "locations": $(".pickup-name-len").eq(0).parent().parent().parent().find("input").eq(2).val(),
            "products": product_list[0]
        }]

        postData.append("pickup_details", JSON.stringify(pickup2_details));
        postData.append("delivery_details", JSON.stringify(address_details));
    }
    // new validation ends here



    postData.append("product_name", JSON.stringify(product_name_push[0]));

    if ($('#pickuptype').val() == 0) {
        $("#snackbarerror").text("Pickup type is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    } else {
        postData.append("pickup_type", $('#pickuptype').val());
    }

    if ($('#deliverytype').val() == 0) {
        $("#snackbarerror").text("Delivery type is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    } else {
        postData.append("delivery_type", $('#deliverytype').val());
    }

    if ($('#weightrange').val() == 0) {
        $("#snackbarerror").text("Weight range is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    } else {
        postData.append("product_weight", $('#weightrange').val());
    }

    if ((addprod_images.length == 0) && (addprod_images.length > 10)) {
        $("#snackbarerror").text("You can add a range of 1 to 10 images only");
        showiperrtoast();
        event.stopPropagation();
        return;
    } else {
        for (i = 0; i < addprod_images.length; i++) {
            postData.append("image", addprod_images[i]);
        }
    }
    if ($("#time").val() != "") {
        postData.append("pickup_time", $('#date_picker1').val() + 'T' + $('#time').val() + ':00');
    }

    if ($('#descriptfadd').val() != '') {
        postData.append("descriptions", $('#descriptfadd').val());
    }
    if ($("#store_name").val() != "") {
        postData.append("store_name", $('#store_name').val());
    }
    if ($("#store_id").val() != "") {
        postData.append("store_code", $('#store_id').val());
    }
    if ($("#pickuptype").val() == "false") {
        if ($("#amount_get").val() != "") {
            postData.append("orderdetails", {
                "product_cost": $('#amount_get').val()
            });
        } else {
            $("#snackbarerror").text("Product Price is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
    } else {
        if ($("#amount_get").val() != "") {
            postData.append("orderdetails", {
                "product_cost": $('#amount_get').val()
            });
        }
    }
    // store_name
    // store_code
    console.log(postData);

    $(".submitbtnord").html('submit <img src="../img/loader.gif">');
    $(".submitbtnord").attr("disabled", true);

    $.ajax({
        url: addorder_api,
        type: 'post',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        headers: {
            "Authorization": "Token " + localStorage.token
        },
        success: function (data) {

            $(".submitbtnord").html('submit');
            $(".submitbtnord").attr("disabled", false);
            $("#snackbarsuccs").text("Order placed");
            showsuccesstoast();
            window.location.href = "order-status.html";
            product_name_push = [];
            product_list = [];
            address_details = [];
            products = [], product_name = [], personal_details_pickup = [], personal_details_delup = [];
        },
        error: function (data) {

            $(".submitbtnord").html('submit');
            $(".submitbtnord").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    })
} //submit functionality ends here

$("#amount_get").hide();
//show store details
function functionshow() {
    if ($("#pickuptype").val() == "false") {
        $("#amount_get").show();
        $(".show_store").show();
    } else {
        $("#amount_get").hide();
        $(".show_store").hide();
    }
}

//replicate function stars here
function replicate(me) {
    if (!$(me).children().hasClass("fa-trash-o")) {
        if ($(me).parent().parent().children().find(".addcatgyreplica").val() == "0" || $(me).parent().parent().parent().children().find(".prod_name").val() == "" || $(me).parent().parent().parent().children().find(".prod_count").val() == "") {
            $("#snackbarerror").text("Enter the Product details to add another product");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        $(".addanother_prod").empty().append(`<i class="fa fa-trash-o" aria-hidden="true"></i>`);
        var lenstring = $(".search-fieldreplica").length;
        $("#show_replica").append(` <div class="row  replicatediv">
        <div class="col-lg-10">
          <div class="form-group form-md-line-input  has-info is-empty ptop0 mb20">
            <select class="form-control ipclkaddemp edited roleclr addcatgyreplica" id="addcatgy">
                              <option value="0">Choose Your Category * </option>${categories}
                          </select>
          </div>
        </div>
        <div class="col-lg-2">
          <a class="btn btn-circle btn-icon-only fsearchrbtn addanother_prod" onclick="replicate(this);">
                        <i class="fa fa-plus" aria-hidden="true"></i></a>
        </div>
      <div class="addsub_replicates">
      <div class="subreplicate">
        <div class="col-md-4">
          <div class="form-group form-md-line-input  has-info ptop0 mb20">
            <input type="text" class="form-control input-sm prod_name" maxlength="30"  id="" placeholder="Product Name*">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group form-md-line-input  has-info ptop0 mb20">
            <input type="text" class="form-control input-sm prod_count" maxlength="2" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="10" id="" placeholder="Product count*">
          </div>
        </div>
        <div class="col-md-4">
          <div class="btn-group btn-group-vertical quantity" data-toggle="buttons">
                                        ${quantities}
                                    </div>
        </div>
        <div class="col-lg-2">
          <a class="btn btn-circle btn-icon-only fsearchrbtn subaddprod" onclick="replicatesub(this);">
                        <i class="fa fa-plus" aria-hidden="true"></i></a>
        </div>
      </div>
    </div>
    </div>`);
    } else {
        $(me).parent().parent().remove();
    }
}

//replicate sub division
function replicatesub(that) {
    if (!$(that).children().hasClass("fa-trash-o")) {
        if ($(that).parent().parent().children().find(".prod_name").val() == "" || $(that).parent().parent().children().find(".prod_count").val() == "" || $(that).parent().parent().parent().prev().prev().find(".addcatgyreplica").val() == "0") {
            $("#snackbarerror").text("Enter the Product details to add another product");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        $(that).parent().parent().parent().parent().find(".subaddprod").remove;
        $(that).parent().parent().parent().parent().find(".addsub_replicates").append(`<div class="subreplicate">
        <div class="col-md-4">
          <div class="form-group form-md-line-input  has-info ptop0 mb20">
            <input type="text" class="form-control input-sm prod_name" maxlength="30"  placeholder="Product Name*">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group form-md-line-input  has-info ptop0 mb20">
            <input type="text" class="form-control input-sm prod_count" maxlength="2" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="10"  placeholder="Product count*">
          </div>
        </div>
        <div class="col-md-4">
        <div class="btn-group btn-group-vertical quantity" data-toggle="buttons">
                                        ${quantities}
                                    </div>
        </div>
      </div> <div class="col-lg-2">
          <a class="btn btn-circle btn-icon-only fsearchrbtn subaddprod" onclick="replicatesub(this);"><i class="fa fa-plus" aria-hidden="true"></i></a>
          <a class="btn btn-circle btn-icon-only fsearchrbtn subaddprod" onclick="replicatesub(this);"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        </div>`);
    } else {
        $(that).parent().prev().remove()
        $(that).parent().remove();
    }
}


//prefill address in keyup
$("#pickuppn").keyup(function () {
    if ($("#pickuppn").val() != "") {
        var postData = JSON.stringify({
            "mobile_number": $("#pickuppn").val()
        })
        $.ajax({
            url: prefill_usraddr_api,
            type: 'POST',
            data: postData,
            headers: {
                "content-type": 'application/json',
                "Authorization": "Token " + localStorage.token
            },
            success: function (data) {
                if (data.length != 0) {
                    console.log(data[data.length - 1])
                    $("#pickupaddr").val(data[data.length - 1].pickup_loc);
                    $("#dropaddr").val(data[data.length - 1].del_loc);
                } else {
                    $("#pickupaddr").val("");
                    $("#dropaddr").val("");
                }
            },
            failure: function (data) {
                for (var key in JSON.parse(data.responseText)) {
                    $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
                }
                showerrtoast();
            }
        })
    }
});
// add address function starts here
function address() {
    $("#warning").modal();
}
var address_details = [], address_showdetails = [], show_product = [], product_list = [], product_name_push = [];
function yes_add_adr() {
    $("#warning").modal();
    if (($(".json-name").val() == "") || ($(".json-number input").val() == "") || ($(".json-addr").val() == "") || ($("#addcatgy").val() == 0) || ($(".prod_count").val() == "") || ($(".prod_name").val() == "")) {
        $("#snackbarerror").text("Enter the Product details to add another address");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    var products = [], personal_details = [], product_name = [];
    for (var i = 0; i < $(".replicatediv").length; i++) {
        for (j = 0; j < $(".replicatediv").eq(i).find(".subreplicate").length; j++) {
            if (($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val() != "") && ($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".quantity .active input").val() != "") && ($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_count").val() != "")) {
                product_name.push({
                    "name": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val(),
                    "category_id": $(".replicatediv").eq(i).find("select").val(),
                    "units_id": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".quantity .active input").val(),
                    "quantity": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_count").val()
                });
                products.push({
                    "category": $(".replicatediv").eq(i).find("select").val(),
                    "name": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val()
                })
            } else {
                $("#snackbarerror").text("Enter the Product details to add another address");
                showiperrtoast();
                event.stopPropagation();
                return;
            }
        }
    }
    personal_details = {
        "name": $(".json-name").val(),
        "mobile": $(".json-number").val(),
        "locations": $(".json-addr").val(),
        "products": products
    };
    show_product = {
        "name": $(".json-name").val(),
        "mobile": $(".json-number").val(),
        "locations": $(".json-addr").val(),
        "products": product_name
    };
    console.log(personal_details);
    //assign the values of existing ones
    var select = $("#single_order select").val();
    var inp1 = $("#single_order input").eq(0).val();
    var inp2 = $("#single_order input").eq(1).val();
    var radio = $("#single_order .quantity input:checked").val();
    $("input[type='radio']").prop('checked', false).parent().attr("disabled",true).removeClass('active').css({"pointer-events":"none"});
    $("#single_order input").prop("disabled",true);
    product_name_push.push(product_name);
    product_list.push(products);
    address_details.push(personal_details);
    address_showdetails.push(show_product);
    if ($("#order_type_val").find(".active input").data("value") == "1") {
        $("#dynamic .topic-dashline").eq(1).empty().append(`<div class="row"><div class="mb-25"><p class="heading_attr p0-1 dis-inline">Pickup Details :</p>
                            <button type="button" onclick="triggermodal()" class="btn btn-circle mbtn mdlfsubmitbtn etrclkpedit viewdetails_btn"><i class="fa fa-info aria-hidden="true"></i> &nbsp;&nbsp;view pickup details<sup class="addsup"></sup></button></div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control json-name input-sm pickup-name-len" id="pickupname" maxlength="50" placeholder="Pickup Name*">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control json-number input-sm" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="15" id="pickuppn" placeholder="Pickup Number*">
                                    </div>
                                </div>
                                <div class="col-md-6" id="pickmul">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm json-addr autocompleteip" id="pickupaddr" maxlength="253" placeholder="Pickup-Address*" style="height: 30px;">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row" id="single_order">
                                <p class="heading_attr p0-1">Product Details :</p>
                                <div class="replicatediv">
                                    <div class="col-lg-10">
                                        <div class="form-group form-md-line-input  has-info is-empty ptop0 mb20">
                                            <select class="form-control ipclkaddemp edited roleclr addcatgyreplica" id="addcatgy">
                                                <option value="0">Choose Your Category *</option>${categories}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn btn-circle btn-icon-only fsearchrbtn addanother_prod" onclick="replicate(this);">
                                            <i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="addsub_replicates">
                                        <div class="subreplicate">
                                            <div class="col-md-4">
                                                <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                                    <input type="text" class="form-control input-sm prod_name" maxlength="30" placeholder="Product Name*">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                                    <input type="text" class="form-control input-sm prod_count" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="2" placeholder="Product count*">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                 <div class="btn-group btn-group-vertical quantity" data-toggle="buttons">
                                        ${quantities}
                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <a class="btn btn-circle btn-icon-only fsearchrbtn subaddprod" onclick="replicatesub(this);">
                                                    <i class="fa fa-plus" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="show_replica"></div>

                                <div><button type="button" onclick="address()" class="btn btn-circle mbtn mdlfsubmitbtn etrclkpedit width100">Add Address</button></div>`);
    } else if ($("#order_type_val").find(".active input").data("value") == "2") {
        $("#dynamic .topic-dashline").eq(1).empty().append(`<div class="row">
                            <div class="mb-25"><p class="heading_attr p0-1 dis-inline">Delivery Details :</p>
                            <button type="button" onclick="triggermodal()" class="btn btn-circle mbtn mdlfsubmitbtn etrclkpedit viewdetails_btn"><i class="fa fa-info aria-hidden="true"></i>&nbsp;&nbsp;view delivery details<sup class="addsup"></sup></button></div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control json-name input-sm delivery-name-len" id="deliveryname" maxlength="50" placeholder="Delivery Name*">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control json-number input-sm" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="15" id="deliverypn" placeholder="Delivery Number*">
                                    </div>
                                </div>
                                <div class="col-md-6" id="delmul">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm json-addr autocompleteip" id="dropaddr" maxlength="253" placeholder="Drop-Address*" style="height: 30px;">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row" id="single_order">
                                <p class="heading_attr p0-1">Product Details :</p>
                                <div class="replicatediv">
                                    <div class="col-lg-10">
                                        <div class="form-group form-md-line-input  has-info is-empty ptop0 mb20">
                                            <select class="form-control ipclkaddemp edited roleclr addcatgyreplica" id="addcatgy">
                                                <option value="0">Choose Your Category *</option>${categories}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn btn-circle btn-icon-only fsearchrbtn addanother_prod" onclick="replicate(this);">
                                            <i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="addsub_replicates">
                                        <div class="subreplicate">
                                            <div class="col-md-4">
                                                <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                                    <input type="text" class="form-control input-sm prod_name" maxlength="30" placeholder="Product Name*">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                                    <input type="text" class="form-control input-sm prod_count" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="2" placeholder="Product count*">
                                                </div>
                                            </div>
                                           <div class="col-md-4">
                                                 <div class="btn-group btn-group-vertical quantity" data-toggle="buttons">
                                        ${quantities}
                                    </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <a class="btn btn-circle btn-icon-only fsearchrbtn subaddprod" onclick="replicatesub(this);">
                                                    <i class="fa fa-plus" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="show_replica"></div>
                                 <div><button type="button" onclick="address()" class="btn btn-circle mbtn mdlfsubmitbtn etrclkpedit width100">Add Address</button></div> `);
    }
    initialize();
    $(".addsup").text("+" + address_details.length);
    console.log(address_details)
    $("#single_order").css({
        "background": "rgba(194, 202, 216, 0.59)",
        "margin": "10px 0"
    })

    $("#single_order select").val(select);
    $("#single_order input").eq(0).val(inp1);
    $("#single_order input").eq(1).val(inp2);
    $("input[type='radio'][value='"+radio+"']").prop('checked', true).parent().addClass('active');
}


//if same address
function no_add_adr() {
    $("#warning").modal();
    if (($(".json-name").val() == "") || ($(".json-number input").val() == "") || ($(".json-addr").val() == "") || ($("#addcatgy").val() == 0) || ($(".prod_count").val() == "") || ($(".prod_name").val() == "")) {
        $("#snackbarerror").text("Enter the Product details to add another address");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    var products = [], personal_details = [], product_name = [];
    for (var i = 0; i < $(".replicatediv").length; i++) {
        for (j = 0; j < $(".replicatediv").eq(i).find(".subreplicate").length; j++) {
            if (($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val() != "") && ($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".quantity .active input").val() != "") && ($(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_count").val() != "")) {
                product_name.push({
                    "name": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val(),
                    "category_id": $(".replicatediv").eq(i).find("select").val(),
                    "units_id": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".quantity .active input").val(),
                    "quantity": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_count").val()
                });
                products.push({
                    "category": $(".replicatediv").eq(i).find("select").val(),
                    "name": $(".replicatediv").eq(i).find(".subreplicate").eq(j).find(".prod_name").val()
                })
            } else {
                $("#snackbarerror").text("Enter the Product details to add another address");
                showiperrtoast();
                event.stopPropagation();
                return;
            }
        }
    }
    personal_details = {
        "name": $(".json-name").val(),
        "mobile": $(".json-number").val(),
        "locations": $(".json-addr").val(),
        "products": products
    };
    show_product = {
        "name": $(".json-name").val(),
        "mobile": $(".json-number").val(),
        "locations": $(".json-addr").val(),
        "products": product_name
    };
    console.log(personal_details);
    product_name_push.push(product_name);
    product_list.push(products);
    address_details.push(personal_details);
    address_showdetails.push(show_product);
    var html = $("#dynamic .topic-dashline").eq(1).find("#single_order").html();
    if ($("#order_type_val").find(".active input").data("value") == "1") {
        $("#dynamic .topic-dashline").eq(1).empty().append(`<div class="row"><div class="mb-25"><p class="heading_attr p0-1 dis-inline">Pickup Details :</p>
                            <button type="button" onclick="triggermodal()" class="btn btn-circle mbtn mdlfsubmitbtn etrclkpedit viewdetails_btn"><i class="fa fa-info aria-hidden="true"></i> &nbsp;&nbsp;view pickup details<sup class="addsup"></sup></button></div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control json-name input-sm pickup-name-len" id="pickupname" maxlength="50" placeholder="Pickup Name*">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control json-number input-sm" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="15" id="pickuppn" placeholder="Pickup Number*">
                                    </div>
                                </div>
                                <div class="col-md-6" id="pickmul">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm json-addr autocompleteip" id="pickupaddr" maxlength="253" placeholder="Pickup-Address*" style="height: 30px;">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row" id="single_order">
                                ${html}
                                </div>
                                <div id="show_replica"></div>

                                <div><button type="button" onclick="address()" class="btn btn-circle mbtn mdlfsubmitbtn etrclkpedit width100">Add Address</button></div>`);
    } else if ($("#order_type_val").find(".active input").data("value") == "2") {
        $("#dynamic .topic-dashline").eq(1).empty().append(`<div class="row">
                            <div class="mb-25"><p class="heading_attr p0-1 dis-inline">Delivery Details :</p>
                            <button type="button" onclick="triggermodal()" class="btn btn-circle mbtn mdlfsubmitbtn etrclkpedit viewdetails_btn"><i class="fa fa-info aria-hidden="true"></i>&nbsp;&nbsp;view delivery details<sup class="addsup"></sup></button></div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control json-name input-sm delivery-name-len" id="deliveryname" maxlength="50" placeholder="Delivery Name*">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control json-number input-sm" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="15" id="deliverypn" placeholder="Delivery Number*">
                                    </div>
                                </div>
                                <div class="col-md-6" id="delmul">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm json-addr autocompleteip" id="dropaddr" maxlength="253" placeholder="Drop-Address*" style="height: 30px;">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row" id="single_order">
                            ${html}
                                </div>
                                <div id="show_replica"></div>
                                 <div><button type="button" onclick="address()" class="btn btn-circle mbtn mdlfsubmitbtn etrclkpedit width100">Add Address</button></div> `);
    }
    initialize();
    $(".addsup").text("+" + address_details.length);
    console.log(address_details)
}


function triggermodal() {
    if (address_showdetails.length == 0) {
        $("#snackbarerror").text("No Address to show");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    $("#list_address").empty();
    for (var i = 0; i < address_showdetails.length; i++) {
        $("#list_address").append(`<div class="row addr_box" id="${i}"><div class="col-md-5"><b>Name &nbsp; :</b>&nbsp; ${address_showdetails[i].name}<br><b> Mobile &nbsp; :</b> &nbsp; ${address_showdetails[i].mobile} <br> <b>Location &nbsp;:</b> &nbsp; ${address_showdetails[i].locations}</div><div class="col-md-5" id="productid${i}"></div><div class="col-md-2"><div class="icon_show"><span class="editicon modal-edit"><i class="fa fa-1x fa-edit" aria-hidden="true"></i></span><span class="delicon modal-edit"><i class="fa fa-1x fa-trash" aria-hidden="true"></i></span></div></div></div>`);
        for (var k = 0; k < address_showdetails[i].products.length; k++) {
            $("#productid" + i).append(`${(k != 0) ? `${($(categories).eq(address_showdetails[i].products[k].category).text() == $(categories).eq(address_showdetails[i].products[k - 1].category).text()) ? `&nbsp;&nbsp; ,<p class="dis-inline">${address_showdetails[i].products[k].name}</p><p class="dis-inline"> - ${address_showdetails[i].products[k].units_id}</p><p class="dis-inline">${$(quantities).find("input").val(address_showdetails[i].products[k].quantity).data("value")}</p>` : `<br><b>${$(categories).eq(address_showdetails[i].products[k].category).text()}:&nbsp; </b><p class="dis-inline">${address_showdetails[i].products[k].name}</p><p class="dis-inline"> - ${address_showdetails[i].products[k].units_id}</p><p class="dis-inline">${$(quantities).find("input").val(address_showdetails[i].products[k].quantity).data("value")}</p>`}` : `<br><b>${$(categories).eq(address_showdetails[i].products[k].category).text()}: &nbsp;</b><p class="dis-inline">${address_showdetails[i].products[k].name}</p><p class="dis-inline"> - ${address_showdetails[i].products[k].units_id}</p><p class="dis-inline">${$(quantities).find("input").val(address_showdetails[i].products[k].quantity).data("value")}</p>`}`);
        }
    }
    $("#address_details").modal();
    //function to del stored addr
$(".delicon").click(function(){
    address_showdetails.splice(0,Number($(this).parent().parent().parent().attr("id")));
    $(this).parent().parent().parent().remove();
    (address_showdetails.length == 0) ?  $("#list_address").append(`<center>No Adddress to show,Please enter the details`) : "";
})

//function for editing the addr
$(".editicon").click(function(){
    $("#pickupname").val(address_showdetails[Number($(this).parent().parent().parent().attr("id"))].name);
    $("#pickuppn").val(address_showdetails[Number($(this).parent().parent().parent().attr("id"))].mobile);
    $("#pickupaddr").val(address_showdetails[Number($(this).parent().parent().parent().attr("id"))].locations);
    $("#addcatgy").val(address_showdetails[Number($(this).parent().parent().parent().attr("id"))].products[0].category_id);
    $(".prod_name").val(address_showdetails[Number($(this).parent().parent().parent().attr("id"))].products[0].name);
    $(".prod_count").val(address_showdetails[Number($(this).parent().parent().parent().attr("id"))].products[0].quantity);
    $(".quantity input[value=" + address_showdetails[Number($(this).parent().parent().parent().attr("id"))].products[0].units_id + "]").prop('checked', true);
    $("#address_details").modal('toggle');
})
}






