//pending orders list dynamic
function listpendingorders(type) {
  sessionStorage.order_type = 2;
  sessionStorage.order_page = type;

  if (type == 0) {
    var url = listorder_api;
    $('#urlloader3').empty().append(`<ul class="list-inline pagination floatright" id="pagination3"></ul>`);

  } else {
    var url = type;
  }

  var postData = JSON.stringify({
    "status": 3,
    "start_date": $("#date_picker").val(),
    "end_date": $("#date_picker1").val(),
    "search": $("#search_query").val()
  });

  $.ajax({
    url: url,
    type: 'POST',
    data: postData,
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      var page_no = 1,
        page_get;
      if (data.next_url != null) {
        var page_get = (data.next_url).split('=');
        var page_no = parseInt(page_get[1]) - 1;
      } else if (data.prev_url != null) {
        var page_get = (data.prev_url).split('=');
        var page_no = parseInt(page_get[1]) + 1;
      } else if ((data.next_url == null) && (data.prev_url == null)) {
        var page_no = 1,
          page_get;
      }
      $('.listpendingorders,.pendemptyimgappend').empty();
      $(".findalldr").hide();
      $(".gobtnall").attr("disabled", false);
      if (data.results.length == 0) {
        $(".pendingclass").hide();
        $(".pendemptyimgappend").empty().append('<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>');
      } else {

        $(".pendingclass").show();
        for (var i = 0; i < data.results.length; i++) {

          if (data.results[i].pickup_image == null || data.results[i].pickup_image.length == 0) {
            var image = '../img/noimage.png';
          } else {
            var image = data.results[i].pickup_image[0].image;
          }

          var created_on = data.results[i].created_on;
          var newdate = created_on.split('T');
          var now = newdate[0].split('-');
          var timeval = Convert_time(newdate[1].slice(0, 8));
          var datenew = now[2] + '/' + now[1] + '/' + now[0];
          if (data.results[i].pickup_time != null) {
            var expected_on = data.results[i].pickup_time;
            var newdate = expected_on.split('T');
            var now = newdate[0].split('-');
            var timeexp_val = Convert_time(newdate[1].slice(0, 8));
            var expect_on = now[2] + '/' + now[1] + '/' + now[0];
            var expected_dateon = expect_on + " " + timeexp_val;
          } else {
            var expected_dateon = "Not Mentioned";
          }

          var picktype = (data.results[i].pickup_type) ? '../img/pick.svg' : '../img/buy.svg';

          $('.listpendingorders').append(`<tr class="appendreadclass${data.results[i].id}"><td>${(page_no == 1)?(page_no) * (i + 1) : (page_no * data.page_size)+ (i + 1)}</td><td>${data.results[i].order_id}</td><td>${data.results[i].pickup_details[0].name}</td><td class="whitenowrap">${datenew} ${timeval}</td><td>${expected_dateon}</td><td><img src=${picktype} class="width50"></td><td onclick="viewproduct(this,${data.results[i].id},0)"><center><img src="${image}" class="img-responsive classborderedimg  tab_img" href="#viewfullimg" data-toggle="modal"><td><a onclick="initMap(${data.results[i].pickup_lat},${data.results[i].pickup_long})" data-toggle="modal" href="#trackingmapmodal" class="btn btn-circle btn-icon-only fsearchrbtn"><i class="icon-info icnsend"></i></a></td><td>${data.results[i].status.name}</td><td><a onclick="morefunction(${data.results[i].id})" class="asntsk">More</a></td></tr>`);

          //<td class="appendcrp${data.results[i].id}"></td>
          // if((data.results[i].status.id == "1") || (data.results[i].status.id == "9")){
          //   $(".appendcrp" + data.results[i].id).append(`<a onclick="changeStatus(${data.results[i].id},10)" class="asntsk">Cancel</a>`);
          // }else if(data.results[i].status.id == "4"){
          //   $(".appendcrp" + data.results[i].id).append(`<a onclick="changeStatus(${data.results[i].id},11)" class="asntsk">Paid</a>`);
          // }else{
          //   $(".appendcrp" + data.results[i].id).append(`<a class="asntsk" disabled="disabled">NA</a>`);
          // }
          (data.results[i].is_read) ? "" : $(".appendreadclass" + data.results[i].id).addClass("readclass_order");
        }
      }
      if ((data.next_url != null) || (data.prev_url != null)) {
        $("#urlloader3").show();

        if (data.next_url != null) {
          var next1 = (data.next_url).split('=');
        } else if (data.prev_url != null) {
          var next1 = (data.prev_url).split('=');
        }
        var val = data.count / data.page_size;
        if (val % 1 === 0) {
          //if number is integer
          var val = val;
        } else {
          var val = parseInt(val) + 1;
        }
        var obj = $('#pagination3').twbsPagination({
          totalPages: val,
          visiblePages: 5,
          onPageClick: function(event, page) {
            console.info(page);
            listpendingorders(next1[0] + "=" + (page));
          }
        });
      } else {
        $("#urlloader3").hide();
      }
    },

    error: function(data) {
      $(".findalldr").hide();
      $(".gobtnall").attr("disabled", false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}

//more function for pending
function morefunction(me) {

  localStorage.userid = me;
  $.ajax({
    url: singleorder_api + me + '/',
    type: 'get',
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      // console.log(data.pickup_type)
      var time_is;
        $("#deliverymodal ol.progtrckr li").css("border-bottom", "4px solid rgba(158, 158, 158, 0.48)");
      if (data.orderdetails.status.id == 7) {
        $("#deliverymodal ol.progtrckr li").eq(0).css("border-bottom", "4px solid red");
        $("#deliverymodal ol.progtrckr li").eq(1).css("border-bottom", "4px solid red");
      } else if (data.orderdetails.status.id == 4) {
        $("#deliverymodal ol.progtrckr li").eq(0).css("border-bottom", "4px solid red");
        $("#deliverymodal ol.progtrckr li").eq(1).css("border-bottom", "4px solid red");
        $("#deliverymodal ol.progtrckr li").eq(2).css("border-bottom", "4px solid red");
      } else if (data.orderdetails.status.id == 5) {
        $("#deliverymodal ol.progtrckr li").eq(0).css("border-bottom", "4px solid red");
        $("#deliverymodal ol.progtrckr li").eq(1).css("border-bottom", "4px solid red");
        $("#deliverymodal ol.progtrckr li").eq(2).css("border-bottom", "4px solid red");
        $("#deliverymodal ol.progtrckr li").eq(3).css("border-bottom", "4px solid red");
      }
      $(".listemp_detailspend").empty();
      $(".orderid_pendltd").text(data.order_id);
      $(".payment_type").text(data.payment_type);
      for (var i = 0; i < data.order_assignment.length; i++) {
        var datecreate = data.order_assignment[i].created_on.split('T')
        var content = datecreate[0].split('-');
        var created_date = content[2] + " " + month[content[1] - 1] + " " + content[0];
        $(".listemp_detailspend").append(`  <tr>
            <td> ${data.order_assignment[i].assign_employee.first_name} </td>
            <td> ${data.order_assignment[i].assign_employee.username}  </td>
            <td> ${(data.order_assignment[i].assign_employee.cancel_reason == null)? "Order Delivered" : data.order_assignment[i].assign_employee.cancel_reason}  </td>
            <td> ${created_date} </td>
            <td> ${data.order_assignment[i].employee.first_name} </td>
          </tr>`);
      }

      var time_taken = (data.orderdetails.time_taken).split(":");

       if (time_taken[0] != "00") {
         time_is = data.orderdetails.time_taken;
        $('.timeexp').text(time_is + " Hours");
      } else if (time_taken[1] != "00") {
         time_is = data.orderdetails.time_taken.slice(3);
        $('.timeexp').text(time_is + " Mins");
      } else {
        $('.timeexp').text('0 Min');
      }


      if (data.pickup_type) {

        $('#paymdet').hide();
        $('.corner').css("border-bottom", "4px solid red");
        $('.corner1').css("border-bottom", "");
        $('#compdivload .idorderpend').text(" " + data.id);
        $('#compdivload .paymentpenddet').text(" " + data.payment_type);
        $('#appendpickupdet .pickername').text(" " + data.pickup_name);
        if (data.pickup_mob == "") {
          $('#appendpickupdet .pickerph_no').text(' Not Mentioned');
        } else {
          $('#appendpickupdet .pickerph_no').text(" " + data.pickup_mob);
        }
        if (data.pickup_loc == "") {
          $('#appendpickupdet .pickerloc').text(' Not Mentioned');
        } else {
          $('#appendpickupdet .pickerloc').text(" " + data.pickup_loc);
        }
        //location of picker to be given
        var picktype = (data.pickup_type) ? 'Pick & Deliver' : 'Buy & Deliver';
        $('.paypick_type').text(" " + picktype);
        $('.delcost').text(" " + parseFloat(data.orderdetails.delivery_cost));
        $('#approdist .dist').text(" " + (data.orderdetails.approx_dist).toFixed(2) + " Kms");
        $('#estimatetime .estimate').text(time_is);
        $('#costval .costvalue').text(" " + parseFloat(data.orderdetails.delivery_cost));
        $('.emp_image').attr('src', data.order_assignment[0].assign_employee.profile_pic);
        $('.dlvboyname').text(" " + data.order_assignment[0].assign_employee.first_name);
        $('.deliverboyyph_no').text(" " + data.order_assignment[0].assign_employee.username);
        $('#appenddeldet .deliveryname').text(" " + data.del_name);
        $('#appenddeldet .deliveryph_no').text(" " + data.del_mob);
        $('#appenddeldet .deliveryloc').text(" " + data.del_loc);
      } else {

        $('#paymdet').show();
        $('.corner').css("border-bottom", "4px solid red");
        $('.corner1').css("border-bottom", "");
        $('#compdivload .idorderpend').text(" " + data.id);
        var picktype = (data.pickup_type) ? 'Pick & Deliver' : 'Buy & Deliver';
        $('.paypick_type').text(" " + picktype);
        $('.delcost').text(" " + (parseFloat(data.orderdetails.delivery_cost) + parseFloat(data.orderdetails.product_cost).toFixed(2)));

        $('#compdivload .paymentpenddet').text(" " + data.payment_type);
        $('#appendpickupdet .pickername').text(" " + data.pickup_name);
        $('#appendpickupdet .pickerph_no').text(" " + data.pickup_mob);
        $('#appendpickupdet .pickerloc').text(" " + data.pickup_loc);
        $('#appenddeldet .deliveryname').text(" " + data.del_name);
        $('#appenddeldet .deliveryph_no').text(" " + data.del_mob);
        $('#appenddeldet .deliveryloc').text(" " + data.del_loc);
        $('#approdist .dist').text(" " + (data.orderdetails.approx_dist).toFixed(2) + " Kms");
        $('#estimatetime .estimate').text(time_is);
        $('#costval .costvalue').text(parseFloat(" " + data.orderdetails.delivery_cost));
        $('#paymdet .paymtdet').text(parseFloat(" " + data.orderdetails.product_cost));
        // $('.emp_image').attr('src', data.order_assignment[0].assign_employee.profile_pic);
        // $('.dlvboyname').text(" " +data.order_assignment[0].assign_employee.first_name);
        // $('.deliverboyyph_no').text(" " +data.order_assignment[0].assign_employee.username);
      }
     
      ((data.descriptions == null) || (data.descriptions == "")) ? $(".description_note").text("Not Mentioned"): $(".description_note").text(data.descriptions);

      $(".product_detailspending").empty();
      var units = ["None", "Kg", "Ltr", "Qty"];

      for (var i = 0; i < data.categories.length; i++) {
        $(".product_detailspending").append(`<b class="aprxdet">${data.categories[i].category.name}</b> <span class="appdcategory${i}"></span><br>`);
        for (var j = 0; j < data.categories[i].products.length; j++) {
          if (j != (data.categories[i].products.length - 1)) {
            $(".appdcategory" + i).append(`${ data.categories[i].products[j].name +"-" +data.categories[i].products[j].quantity + " " + units[parseInt(data.categories[i].products[j].units) - 1] },`);
          } else {
            $(".appdcategory" + i).append(`${ data.categories[i].products[j].name +"-" +data.categories[i].products[j].quantity + " " + units[parseInt(data.categories[i].products[j].units) - 1] }`);
          }
        }
      }
      $("#deliverymodal").modal();
      listpendingorders(0);

    },
    error: function() {
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}

function myMap(lat, long) {
  // var myCenter = new google.maps.LatLng(51.508742, -0.120850);
  var myCenter = new google.maps.LatLng(lat, long);

  var mapCanvas = document.getElementById("map");
  var mapOptions = {
    center: myCenter,
    zoom: 15
  };
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var marker = new google.maps.Marker({
    position: myCenter
  });
  marker.setMap(map);
}

// setInterval(function(){
//   if($("#trackingmapmodal:visible").length == 1){
//     $("area").click();
//   }
// },10);

function initMap(lati, longi) {
  // load_mapon();
  var myLatlng = {
    lat: lati,
    lng: longi
  };

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
    center: myLatlng
  });

  var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    title: 'Click to zoom'
  });

  map.addListener('center_changed', function() {
    // 3 seconds after the center of the map has changed, pan back to the
    // marker.
    window.setTimeout(function() {
      map.panTo(marker.getPosition());
    }, 3000);
  });

  marker.addListener('click', function() {
    map.setZoom(15);
    map.setCenter(marker.getPosition());
  });

  // google.maps.event.addDomListener(window, "load", myMap);
  setTimeout(function() {
    $("area").click();
  }, 100);
}


function load_mapon() {
  var head = document.getElementsByTagName('head')[0];
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDcLJZFw-wUw3dk1NI-1E7aDLX312vIt_4&callback=myMap';
  head.appendChild(script);
}

//function to change status
function changeStatus(id, val) {
  var postData = JSON.stringify({
    "status": val
  })
  $.ajax({
    url: addorder_api + id + '/change_status/',
    type: 'PUT',
    data: postData,
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    }
  }).done(function(data) {
    listpendingorders(0);
  }).fail(function(data) {
    for (var key in JSON.parse(data.responseText)) {
      $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
    }
    showerrtoast();
  })
}
