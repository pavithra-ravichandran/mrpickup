// initial func starts here
var url;
$(function() {
    loadcategory();
});

function loadcategory(type) {
    (type == 0) ? url = listcategory_api: url = listcategory_api + "?date=" + $("#date_picker").val();
    $(".findalldr").hide();
    $(".findallbtn").attr("disabled", false);
    $.ajax({
        url: url,
        type: "GET",
        headers: {
            "Authorization": "token " + localStorage.token,
            "content-type": "application/json"
        }
    }).done(function(data) {
        $(".categorytbleload,.emptyimgappend").empty();
        if (data.length != 0) {
            $(".categoryhead").show();
            for (var i = 0; i < data.length; i++) {
                var datecreate = data[i].create_on.split('T')
                var content = datecreate[0].split('-');
                var created_date = content[2] + " " + month[content[1] - 1] + " " + content[0];
                $(".categorytbleload").append(`<tr>
        <td> ${i+1} </td>
        <td class="categoryname${data[i].id}"> ${data[i].name} </td>
        <td> ${created_date} </td>
        <td> ${(data[i].is_active) ? '<span class="badge badge-warning">Active </span>' : '<span class="badge badge-error">Inactive </span>'} </td>
        <td>
            <a onclick="editcategory_id(${data[i].id})" class="btn btn-circle btn-icon-only fsearchrbtn"><i class="icon-note icnsend"></i></a>&emsp;<a onclick="deletecommon(0,${data[i].id})" class="btn btn-circle btn-icon-only fsearchrbtn hide_del"><i class="icon-trash icnsend"></i></a>
        </td>
    </tr>`);
            }
            (localStorage.used_id == "1") ? "" : $(".hide_del").hide();
        } else {
            $(".categoryhead").hide();
            $('.emptyimgappend').empty().append(`<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>`);
        }
    }).fail(function(data) {
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    })
}

//save category starts here
function savecategory() {
    if ($("#savecategy").val() == "") {
        $("#snackbarerror").text("Category name is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }


    var postData = JSON.stringify({
        "name": $("#savecategy").val()
    })

    $(".saveldr").show();
    $(".savebtn").attr("disabled", true);
    $.ajax({
        url: listcategory_api,
        data: postData,
        type: "POST",
        headers: {
            'content-type': "application/json",
            'Authorization': "token " + localStorage.token
        }
    }).done(function(data) {
        $(".saveldr").hide();
        $(".savebtn").attr("disabled", false);
        $("#savecategy").val("");
        loadcategory(0);
        $(".closecategorymdl").click();
        $("#snackbarsuccs").text("Category Added Successfully!");
        showsuccesstoast();
    }).fail(function(data) {
        $(".saveldr").hide();
        $(".savebtn").attr("disabled", false);
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    })
}
//category save ends here

//edit category starts here
function editcategory_id(id) {
    edit_id = id;
    $(".categorynameedit_class").focusout();
    $("#editcategoryname_id").val($(".categoryname" + id).text());
    $("#editcategorynamemodal").modal();
}

function editcategory() {
    if ($("#editcategoryname_id").val() == "") {
        $("#snackbarerror").text("Category name is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }


    var postData = JSON.stringify({
        "name": $("#editcategoryname_id").val()
    })

    $(".changeldr").show();
    $(".changebtn").attr("disabled", true);
    $.ajax({
        url: listcategory_api + edit_id + '/',
        data: postData,
        type: "PUT",
        headers: {
            'content-type': "application/json",
            'Authorization': "token " + localStorage.token
        }
    }).done(function(data) {
        $(".changeldr").hide();
        $(".changebtn").attr("disabled", false);
        loadcategory(0);
        $(".editcatclosemodal").click();
        $("#snackbarsuccs").text("Category Edited Successfully!");
        showsuccesstoast();
    }).fail(function(data) {
        $(".changeldr").hide();
        $(".changebtn").attr("disabled", false);
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    })
}

//dele starts here
function deletecommon(val, id) {
    del_type = val;
    del_id = id;
    $("#deletemodal").modal();
}

//delete category stars here
function deletecategory() {
    $(".delldr").show();
    $(".delbtn").attr("disabled", true);
    if (del_type == 0) {
        var url = listcategory_api + del_id + "/safe_delete/";
    } else if (del_type == 1) {
        var url = listrate_api + del_id + "/safe_delete/";
    } else {
        var url = offers_api + del_id + "/safe_delete/";
    }
    $.ajax({
        url: url,
        type: "DELETE",
        headers: {
            "content-type": "application/json",
            "Authorization": "token " + localStorage.token
        }
    }).done(function(data) {
        $(".delldr").hide();
        $(".delbtn").attr("disabled", false);
        if (del_type == 0) {
            loadcategory(0);
            $("#snackbarsuccs").text("Category Status Changed Successfully!");
        } else if (del_type == 1) {
            loadratecard(0)
            $("#snackbarsuccs").text("Rate card Status Changed Successfully!");
        } else {
            listoffers(0);
            $("#snackbarsuccs").text("Offer Status Changed Successfully!");
        }
        showsuccesstoast();
        $(".delcatclose").click();
    }).fail(function(data) {
        $(".delldr").hide();
        $(".delbtn").attr("disabled", false);
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    })
}


//load rate card starts here
function loadratecard(type) {
    (type == 0) ? url = listrate_api: url = listrate_api + "?date=" + $("#date_picker").val();
    $(".findalldr").hide();
    $(".findallbtn").attr("disabled", false);
    $.ajax({
        url: url,
        type: "GET",
        headers: {
            "Authorization": "token " + localStorage.token,
            "content-type": "application/json"
        }
    }).done(function(data) {
        $(".rateappendimg,.ratetbody").empty();
        if (data.length != 0) {
            $(".ratethead").show();
            for (var i = 0; i < data.length; i++) {
                var datecreate = data[i].create_on.split('T')
                var content = datecreate[0].split('-');
                var created_date = content[2] + " " + month[content[1] - 1] + " " + content[0];
                $(".ratetbody").append(`<tr class="activerate${data[i].id}">
            <td> ${i+1} </td>
            <td > <span class="append${data[i].id}">${data[i].from_weight} </span>kg </td>
            <td > <span class="append${data[i].id}">${data[i].to_weight}</span> kg </td>
            <td> Rs. <span  class="append${data[i].id}">${data[i].distance_cost}</span> </td>
            <td >  <span class="append${data[i].id}">${data[i].extra_km}</span> Km</td>
            <td >  <span class="append${data[i].id}">${data[i].extra_kg}</span> Kg</td>
            <td><span class="append${data[i].id}">  ${data[i].max_km} </span> Km</td>
            <td>Rs. <span class="append${data[i].id}">  ${data[i].extra_delivery_cost} </span> </td>
            <td><span class="append${data[i].id}">  ${data[i].extra_delivery_km} </span> Km</td>
            <td>Rs. <span class="append${data[i].id}">  ${data[i].extra_delivery_km_cost} </span> </td>
            <td>${created_date}</td>
            <td class="del_type${data[i].id}" value="${data[i].delivery_type}">${(data[i].delivery_type == "UR")? "Urgent" : "Ordinary"}</td>
            <td>
                <a onclick="editrank_id(${data[i].id})" class="btn btn-circle btn-icon-only fsearchrbtn"><i class="icon-note icnsend"></i></a>&emsp;<a onclick="deletecommon(1,${data[i].id})" class="btn btn-circle btn-icon-only fsearchrbtn"><i class="icon-trash icnsend"></i></a>
            </td>
        </tr>`);
                (data[i].is_active) ? "" : $(".activerate" + data[i].id).addClass("inactive");

            }
        } else {
            $(".ratethead").hide();
            $('.rateappendimg').empty().append(`<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>`);
        }
    }).fail(function(data) {
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    })
}

//add offers starts here
function addratecards() {
    for (var i = 0; i < $(".addratecardclass").length; i++) {
        if ($(".addratecardclass").eq(i).val() == "") {
            $("#snackbarerror").text($(".addrateerrclass").eq(i).text().replace("*", " ") + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    var postData = JSON.stringify({
        "to_weight": $(".addratecardclass").eq(1).val(),
        "from_weight": $(".addratecardclass").eq(0).val(),
        "distance_cost": $(".addratecardclass").eq(2).val(),
        "extra_km": $(".addratecardclass").eq(3).val(),
        "extra_kg": $(".addratecardclass").eq(4).val(),
        "delivery_type": $(".addratecardclass").eq(5).val(),
        "max_km": $(".addratecardclass").eq(6).val(),
        "extra_delivery_cost": $(".addratecardclass").eq(7).val(),
        "extra_delivery_km": $(".addratecardclass").eq(8).val(),
        "extra_delivery_km_cost": $(".addratecardclass").eq(9).val()

    });

    $(".addrateldr").show();
    $(".addratebtn").attr("disabled", true);

    $.ajax({
        url: listrate_api,
        data: postData,
        type: "POST",
        headers: {
            'content-type': "application/json",
            'Authorization': "token " + localStorage.token
        }
    }).done(function(data) {
        $(".addrateldr").hide();
        $(".addratebtn").attr("disabled", false);
        loadratecard(0);
        $(".ratecardaddclose").click();
        $("#snackbarsuccs").text("Rate Card has been added successfully!");
        showsuccesstoast();
        $(".addratecardclass").val("");
    }).fail(function(data) {
        $(".addrateldr").hide();
        $(".addratebtn").attr("disabled", false);
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    })

}

//edit rank function starts here
function editrank_id(id) {
    edit_id = id;
    for (var i = 0; i < 10; i++) {
        // if (i != 5) {
            $(".editrateclass").eq(i).val($(".append" + id).eq(i).text()).focusout();
        // }
        //  else {
        //     $(".editrateclass").eq(6).val($(".append" + id).eq(5).text()).focusout();
        // }
    }

    $("#order_typedit").val($(".del_type" + id).attr("value"))
    $("#editratecardmodal").modal();
}


function editrank() {
    for (var i = 0; i < $(".editrateclass").length; i++) {
        if ($(".editrateclass").eq(i).val() == " ") {
            $("#snackbarerror").text($(".label_errclass").eq(i).text().replace("*", " ") + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    var postData = JSON.stringify({
        "to_weight": $(".editrateclass").eq(1).val(),
        "from_weight": $(".editrateclass").eq(0).val(),
        "distance_cost": $(".editrateclass").eq(2).val(),
        "extra_km": $(".editrateclass").eq(3).val(),
        "extra_kg": $(".editrateclass").eq(4).val(),
        "max_km": $(".editrateclass").eq(5).val(),
        "extra_delivery_cost": $(".editrateclass").eq(6).val(),
        "extra_delivery_km": $(".editrateclass").eq(7).val(),
        "extra_delivery_km_cost": $(".editrateclass").eq(8).val(),
        "delivery_type" : $("#order_typedit").val()
    })

    $(".changerateldr").show();
    $(".changeratebtn").attr("disabled", true);
    $.ajax({
        url: listrate_api + edit_id + '/',
        data: postData,
        type: "PUT",
        headers: {
            'content-type': "application/json",
            'Authorization': "token " + localStorage.token
        }
    }).done(function(data) {
        $(".changerateldr").hide();
        $(".changeratebtn").attr("disabled", false);
        loadratecard(0);
        $(".ratecardclose").click();
        $("#snackbarsuccs").text("Rate Card Edited Successfully!");
        showsuccesstoast();
    }).fail(function(data) {
        $(".changerateldr").hide();
        $(".changeratebtn").attr("disabled", false);
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    })
}


//list offers starts here
function listoffers(type) {
    (type == 0) ? url = offers_api: url = offers_api + "?date=" + $("#date_picker").val();
    $(".findalldr").hide();
    $(".findallbtn").attr("disabled", false);
    $.ajax({
        url: url,
        type: "GET",
        headers: {
            "Authorization": "token " + localStorage.token,
            "content-type": "application/json"
        }
    }).done(function(data) {
        $(".offerstbody,.offersimgappend").empty();
        if (data.length != 0) {
            $(".offersthead").show();
            for (var i = 0; i < data.length; i++) {
                var datecreate = data[i].create_on.split('T')
                var content = datecreate[0].split('-');
                var created_date = content[2] + " " + month[content[1] - 1] + " " + content[0];
                // {
                //      "id": 1,
                //      "name": "Dewali Offer",
                //      "from_price": "500.00",
                //      "to_price": "1000.00",
                //      "offer_km": "100.00",
                //      "offer_available": 3,
                //      "is_active": true,
                //      "create_on": "2018-01-03T15:08:55.162208"
                //  }
                if (data[i].offer_available == 1) {
                    var offer_type = "Buy & Deliver";
                } else if (data[i].offer_available == 2) {
                    var offer_type = "Pick & Deliver";
                } else {
                    var offer_type = "Both";
                }
                $(".offerstbody").append(`<tr class="activeoffer${data[i].id}">
            <td> ${i+1} </td>
            <td><span class="offerappend${data[i].id}"> ${data[i].name}</span> </td>
            <td>Rs.<span class="offerappend${data[i].id}">  ${data[i].from_price}</span></td>
            <td>Rs.<span class="offerappend${data[i].id}">  ${data[i].to_price}</span></td>
            <td><span class="offerappend${data[i].id}">  ${data[i].offer_km} </span> Km</td>
            <td> <span class="offerappend${data[i].id}" style="display:none">${data[i].offer_available}</span> ${offer_type} </td>
            <td> ${created_date} </td>
            <td>
                <a onclick='editoffer_id(${data[i].id})' class="btn btn-circle btn-icon-only fsearchrbtn"><i class="icon-note icnsend"></i></a>&emsp;<a onclick="deletecommon(2,${data[i].id})" class="btn btn-circle btn-icon-only fsearchrbtn"><i class="icon-trash icnsend"></i></a>
            </td>
        </tr>`);
                (data[i].is_active) ? "" : $(".activeoffer" + data[i].id).addClass("inactive");

            }
        } else {
            $(".offersthead").hide();
            $('.offersimgappend').empty().append(`<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>`);
        }
    }).fail(function(data) {
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    })
}

//save or add offers starts here
function addoffers() {
    for (var i = 0; i < $(".addoffers_class").length; i++) {
        if ($(".addoffers_class").eq(i).val() == "") {
            $("#snackbarerror").text($(".label_addoffers").eq(i).text().replace("*", " ") + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    var postData = JSON.stringify({
        "to_price": $(".addoffers_class").eq(2).val(),
        "name": $(".addoffers_class").eq(0).val(),
        "from_price": $(".addoffers_class").eq(1).val(),
        // "offer_price": $(".addoffers_class").eq(3).val(),
        "offer_km": $(".addoffers_class").eq(3).val(),
        "offer_available": $(".addoffers_class").eq(4).val()
    });

    $(".addofferldr").show();
    $(".addofferbtn").attr("disabled", true);

    $.ajax({
        url: offers_api,
        data: postData,
        type: "POST",
        headers: {
            'content-type': "application/json",
            'Authorization': "token " + localStorage.token
        }
    }).done(function(data) {
        $(".addofferldr").hide();
        $(".addofferbtn").attr("disabled", false);
        listoffers(0);
        $(".offerclosemodal").click();
        $("#snackbarsuccs").text("Offer has been added successfully!");
        showsuccesstoast();
        $(".addoffers_class").val("");
    }).fail(function(data) {
        $(".addofferldr").hide();
        $(".addofferbtn").attr("disabled", false);
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    })
}


//edit offers starts here
function editoffer_id(id) {
    edit_id = id;
    for (var i = 0; i < $(".editofferclass").length; i++) {
        $(".editofferclass").eq(i).val($(".offerappend" + id).eq(i).text()).focusout();
    }
    $("#editoffersmodal").modal();
}

function editoffers() {
    var postData = JSON.stringify({
        "to_price": $(".editofferclass").eq(2).val(),
        "name": $(".editofferclass").eq(0).val(),
        "from_price": $(".editofferclass").eq(1).val(),
        "offer_km": $(".editofferclass").eq(3).val(),
        "offer_available": $(".editofferclass").eq(4).val()
    });

    $(".editofferldr").show();
    $(".editofferbtn").attr("disabled", true);

    $.ajax({
        url: offers_api + edit_id + '/',
        data: postData,
        type: "PUT",
        headers: {
            'content-type': "application/json",
            'Authorization': "token " + localStorage.token
        }
    }).done(function(data) {
        $(".editofferldr").hide();
        $(".editofferbtn").attr("disabled", false);
        listoffers(0);
        $(".offereditclose").click();
        $("#snackbarsuccs").text("Offer edited successfully!");
        showsuccesstoast();
    }).fail(function(data) {
        $(".editofferldr").hide();
        $(".editofferbtn").attr("disabled", false);
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    })
}


//search function strat shere
function searchnow() {
    if ($(".tab_clr.active").find("a").data("attr_val") == 0) {
        loadcategory(1);
    } else if ($(".tab_clr.active").find("a").data("attr_val") == 1) {
        loadratecard(1);
    } else {
        listoffers(1);
    }
}


function searchall() {
    $(".findalldr").show();
    $(".findallbtn").attr("disabled", true);
    $("#date_picker").val("");
    if ($(".tab_clr.active").find("a").data("attr_val") == 0) {
        loadcategory(0);
    } else if ($(".tab_clr.active").find("a").data("attr_val") == 1) {
        loadratecard(0);
    } else {
        listoffers(0);
    }
}
