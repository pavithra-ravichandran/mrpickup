//initial function starts HERE
$(function() {
  loaddetails(0);
});


function loaddetails(type) {
  if (type == 0) {
    var url = addemployees_api + sessionStorage.employee_id + '/retrieve_employee/';
  } else {
    var url = type;
  }
  $.ajax({
    url: url,
    type: "POST",
    data: JSON.stringify({
      "date": $("#date_picker").val(),
      "search": $("#search_query").val()
    }),
    headers: {
      "content-type": "application/json",
      "Authorization": "token " + localStorage.token
    }
  }).done(function(data) {
    $(".findbtnall").attr("disabled", false);
    $(".findalldr").hide();
    $(".employeelistclass,.starzappend,.imgemptyappend").empty();
    $(".Amtclass").text("Rs. " + (data.due_amount).toFixed(2));

    (data.employee_deatils.userprofile.profile_pic != null) ? $(".profile_img").attr("src", data.employee_deatils.userprofile.profile_pic): "";
    var datejoined = data.employee_deatils.date_joined.split('T')
    var content = datejoined[0].split('-');
    var datejoin = content[2] + " " + month[content[1] - 1] + " " + content[0];
    $(".employeename").text(data.employee_deatils.first_name);
    $(".join_date").text(datejoin);
    var star = data.employee_deatils.userprofile.rating;
    if (star) {
      var whole = Math.floor(star);
      var high = 5 - Math.ceil(star);
      var dec = (star * 10) % 10;
      for (var i = 0; i < whole; i++) {
        $(".starzappend").append(`
                     <i class="fa fa-star purplecolor" aria-hidden="true"></i>
                 `);
      }
      if (dec > 4) {
        $(".starzappend").append(`
                    <i class="fa fa-star-half-o purplecolor" aria-hidden="true"></i>`);
      } else if (dec < 5 && dec != 0) {
        $(".starzappend").append(`
                    <i class="fa fa-star-o purplecolor" aria-hidden="true"></i>
                    `);
      }
      for (var i = 0; i < high; i++) {
        $(".starzappend").append(`
                    <i class="fa fa-star-o purplecolor" aria-hidden="true"></i>
                      `);
      }
    } else {
      for (var i = 0; i < 5; i++) {
        $(".starzappend").append(`
                    <i class="fa fa-star-o purplecolor" aria-hidden="true"></i>
                      `);
      }
    }
    if (data.results.length != 0) {
      $(".empthead").show();
      var page = url.split("=");

      for (var i = 0; i < data.results.length; i++) {
        var odrdate = data.results[i].created_on.split('T')
        var content = odrdate[0].split('-');
        var order = content[2] + " " + month[content[1] - 1] + " " + content[0];
        $(".employeelistclass").append(`<tr>
  <td> ${i + 1 * parseInt((page[1] == undefined) ? "1" : page[1] )} </td>
  <td>${data.results[i].order.order_id}</td>
  <td>${data.results[i].order.pickup_loc}</td>
  <td>${data.results[i].order.del_loc}</td>
  <td>${order}</td>
  <td>${data.results[i].order.orderdetails.approx_dist}</td>
  <td>${data.results[i].order.orderdetails.time_taken}</td>
  <td> Rs.${data.results[i].order.orderdetails.total_cost}</td>
  <td>${data.results[i].order.orderdetails.status}</td>
  <td>${data.results[i].order.rating}</td>
  <td><a onclick="customerdetailpage(${data.results[i].order.customer.id})">${data.results[i].order.customer.name}</a></td>
</tr>`);
      }
    } else {
      $(".empthead").hide();
      $(".imgemptyappend").empty().append('<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>');
    }

    if (data.next_url != null) {
      var next1 = (data.next_url).split('=');
      var val = data.count / data.page_size;
      if (val % 1 === 0) {
        //if number is integer
        var val = val;
      } else {
        var val = parseInt(val) + 1;
      }
      var obj = $('#pagination').twbsPagination({
        totalPages: val,
        visiblePages: 5,
        onPageClick: function(event, page) {
          console.info(page);
          loaddetails(next1[0] + "=" + (page));
        }
      });
    }

  }).fail(function(data) {
    for (var key in JSON.parse(data.responseText)) {
      $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
    }
    showerrtoast();
  })
}

//find function starts here
function find() {
  $('#urlloader').empty();
  $('#urlloader').append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
  loaddetails(0);
}
//findall function starts here
function findall() {
  $('#urlloader').empty();
  $('#urlloader').append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
  $(".findbtnall").attr("disabled", true);
  $(".findalldr").show();
  $("#date_picker").val("");
  loaddetails(0);
}

//search on keyup starts here
$("#search_query").keyup(function() {
  find();
})

//function for customer detailed page
function customerdetailpage(id) {
  sessionStorage.customer_id = id;
  window.location.href = "customerdetail.html";
}


//list clearance functionality strats HERE
function submitclearance() {
  if ($("#fineamt").val() == "") {
    $("#snackbarerror").text("Amount is required");
    showerrtoast();
    event.stopPropagation();
    return;
  } else if ($("#descriptionclassmodal").val() == "") {
    $("#snackbarerror").text("Description is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  $.ajax({
    url: rateclearance_api,
    type: "POST",
    data: JSON.stringify({
      //     "employee_id":"",
      //     "description":"",
      //    "paid_amount":""
      "paid_amount": $("#fineamt").val(),
      "employee_id": sessionStorage.employee_id,
      "description": $("#descriptionclassmodal").val()
    }),
    headers: {
      "content-type": "application/json",
      "Authorization": "token " + localStorage.token
    }
  }).done(function(data) {
    $(".modalclose").click();
    $("#fineamt,#descriptionclassmodal").val("");
    $("#snackbarsuccs").text("Amount Delivered");
    showsuccesstoast();
    $(".Amtclass").text("Rs. " + (data.total_amount).toFixed(2));
    // loaddetails(0);
  }).fail(function(data) {
    for (var key in JSON.parse(data.responseText)) {
      $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
    }
    showerrtoast();
  })
}



//modal for list clearance
function listclerance_amount() {
  $.ajax({
    url: rateclearance_api + sessionStorage.employee_id + "/last_transaction/" ,
    type: "GET",
    headers: {
      "content-type": "application/json",
      "Authorization": "token " + localStorage.token
    }
  }).done(function(data) {
    // var arr = ["total_amount", "paid_amount","description"];
    if(data.paid_amount != null){
    var prv_bal= parseFloat(data.total_amount) - parseFloat(data.paid_amount);
    var datecreate = data.created_on.split('T')
    var content = datecreate[0].split('-');
    var created_date = content[2] + " " + month[content[1] - 1] + " " + content[0];
    $(".earnedclassmodal").eq(6).text($(".Amtclass").text());
    $(".earnedclassmodal").eq(0).text(data.total_amount);
    $(".earnedclassmodal").eq(1).text(data.paid_amount);
    $(".earnedclassmodal").eq(2).text(prv_bal);
    $(".earnedclassmodal").eq(3).text(created_date);
    $(".earnedclassmodal").eq(4).text(data.created_by.first_name + " " + data.created_by.last_name);
    $(".earnedclassmodal").eq(5).text(data.description);
    }

    $("#listclearancemodal").modal();
  }).fail(function(data) {
    for (var key in JSON.parse(data.responseText)) {
      $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
    }
    showerrtoast();
  })
}
