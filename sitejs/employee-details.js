//variable dec;are
var roleemp = ["Delivery Boy", "Collection man", "Sub Admin"],
  userprofile = [],
  useraddress = [],emp_id;
//employee page srcipt starts here
$(function() {
  listemployeedetails(0);
  loadempdetails();
})

function listemployeedetails(type) {
  var postData = JSON.stringify({
    "role":((($("#filter_list").val() == 0) || ($("#filter_list").val() == null)) ? "" : $("#filter_list").val()),
    "search": $('.srchbynm').val(),
    "is_active" : $("input[name='view']:checked").attr("value")
  });

  if (type == 0) {
    url = listemployees_api;
  } else {
    url = type;
  }

  $.ajax({
    url: url,
    type: "POST",
    data: postData,
    headers: {
      "Authorization": "Token " + localStorage.token,
      "content-type": 'application/json'
    },
    success: function(data) {
      $(".srch_nm").html('<i class="icon-action-redo"></i>');
      $(".srch_all").html('Find All');
      $(".srch_nm,.srch_all").attr("disabled", false);
      $('.dyn_empdtl,.emptyimg').empty();
      if (data.results.length != 0) {
        $(".cssTable,.empdet").show();
        for (var i = 0; i < data.results.length; i++) {
          if (data.results[i].useraddress != null) {
            var street = (data.results[i].useraddress.street != null) ? data.results[i].useraddress.street : '';
            var area = (data.results[i].useraddress.area != null) ? data.results[i].useraddress.area : '';
            var city = (data.results[i].useraddress.city != null) ? data.results[i].useraddress.city : '';
            var state = (data.results[i].useraddress.state != null) ? data.results[i].useraddress.state : '';
            var country = (data.results[i].useraddress.country != null) ? data.results[i].useraddress.country : '';
            var zipcode = (data.results[i].useraddress.zipcode != null) ? data.results[i].useraddress.zipcode : '';
          }

          $('.dyn_empdtl').append(` <tr class="trclass${i}">
                                                                <td> ${data.results[i].id} </td>
                                                                <td onclick="viewImg(this);">
                                                                    <center><img class="img-responsive classimg cptr" src="${data.results[i].userprofile.profile_pic == null? '../img/dummyprofile.jpg' :data.results[i].userprofile.profile_pic}" href="#viewfullimg" data-toggle="modal"></center>
                                                                </td>
                                                                <td><a onclick="gotodetailedpage(${data.results[i].id})">
                                                                   ${data.results[i].first_name == "" ? "EmployeeId " + data.results[i].id : data.results[i].first_name}</a>
                                                                </td>

                                                                <td> ${data.results[i].username}</td>
                                                                <td>${data.results[i].email}</td>
                                                                <td>${data.results[i].userprofile.dob}</td>
                                                                <td>
                                                                   ${street}, ${area}, ${city}, ${state}, ${country}, ${zipcode}
                                                                </td>
                                                                <td value=${data.results[i].userprofile.role.id}><strong>${data.results[i].userprofile.role.name}</strong></td>
                                                                <td>
                                                                    <a onclick="editdata(this,'${data.results[i].userprofile.proof_image}')" data-toggle="modal" href="#editzone" class="btn btn-circle btn-icon-only fsearchrbtn">
                                                                        <i class="icon-note icnsend"></i>
                                                                    </a>
                                                                    <a data-toggle="modal" href="#confirmmodal" class="btn btn-circle btn-icon-only fsearchrbtn" onclick="deactemp(this);">
                                                                        <i class="icon-trash icnsend"></i>
                                                                    </a>
                                                                      <a data-toggle="modal" href="#resetsubpassmodal" class="btn btn-circle btn-icon-only fsearchrbtn" onclick="emp_id(${data.results[i].id});"><i class="fa fa-key" style="font-size: 19px;"></i></a>
                                                                </td>
                                                                <td onclick="viewProof(this,'${data.results[i].userprofile.proof_image}');">
                                                                    <a data-toggle="modal" href="#viewfullimg_emp" class="btn btn-circle btn-icon-only fsearchrbtn">
                                                                        <i class="icon-info icnsend"></i>
                                                                    </a>
                                                                </td>

                                                            </tr>`);
          (data.results[i].userprofile.on_leave) ? $('.trclass' + i).addClass('subadmin'): '';
          (data.results[i].is_active == false) ? $('.trclass' + i).addClass('inactive'): '';

        } //for loop ends here
      } else {
        $(".cssTable,.empdet").hide();
        $(".emptyimg").empty().append('<center><img id="theImg" src="../img/ndf.png" height="400px" width="400px"/>');
      }
      if (data.next_url != null) {
        var next1 = (data.next_url).split('=');
        var val = data.count / data.page_size;
        if (val % 1 === 0) {
          //if number is integer
          var val = val;
        } else {
          var val = parseInt(val) + 1;
        }
        var obj = $('#pagination').twbsPagination({
          totalPages: val,
          visiblePages: 5,
          onPageClick: function(event, page) {
            console.info(page);
            listemployeedetails(next1[0] + "=" + (page));
          }
        });
      }
      //pagination ends here
    },
    error: function(data) {
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  })

} //func ends here


//add employees fn starts here
function addEmployee() {

  var postData = new FormData();
  //validations starts here
  if ($('#addempname').val().trim() == '') {
    $("#addempname").focus();
    $("#snackbarerror").text("Employee name is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  if ($('#addempmailid').val().trim() == '') {
    $("#addempmailid").focus();
    $("#snackbarerror").text("Employee Email id is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  var email = $('#addempmailid').val();
  var atpos = email.indexOf("@");
  var dotpos = email.lastIndexOf(".");
  if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
    $("#addempmailid").focus();
    $("#snackbarerror").text("Enter a valid Email id");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#addempphoneno').val().trim() == '') {
    $("#addempphoneno").focus();
    $("#snackbarerror").text("Employee Phone number is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  if ($('.addempdob').val().trim() == '') {
    $("#snackbarerror").text("Date of Birth is required");
    showerrtoast();
    $(".addempdob").focus();
    event.stopPropagation();
    return;
  }
  var currentyear = new Date().getFullYear();
  var usergivenyear = parseInt($('.addempdob').val().split("-")[0]);
  if (currentyear - usergivenyear < 18) {
    $("#snackbarerror").text("Your age should be above 18 to register");
    showerrtoast();
    $(".addempdob").focus();
    event.stopPropagation();
    return;
  }
  if ($('#addempstreet').val().trim() == '') {
    $("#addempstreet").focus();
    $("#snackbarerror").text("Employee's street is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#addemparea').val().trim() == '') {
    $("#addemparea").focus();
    $("#snackbarerror").text("Employee's area is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#addempcity').val().trim() == '') {
    $("#addempcity").focus();
    $("#snackbarerror").text("Employee's city is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#addempstate').val().trim() == '') {
    $("#addempstate").focus();
    $("#snackbarerror").text("Employee's state is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#addempcountry').val().trim() == '') {
    $("#addempcountry").focus();
    $("#snackbarerror").text("Employee's Country is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#addempzipcode').val().trim() == '') {
    $("#addempzipcode").focus();
    $("#snackbarerror").text("zipcode is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#addemprole').val() == 0) {
    $("#snackbarerror").text("Employee's role is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($("#addadminpassword").val().trim() == '') {
    $("#addadminpassword").focus();
    $("#snackbarerror").text("Admin Password is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#addemppassword').val().trim() == '') {
    $("#addemppassword").focus();
    $("#snackbarerror").text("Employee's Password is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  var empprofimagefile = $(".pro_pic")[0].files;

  var empvoderidfile = $(".proof_pic")[0].files;

  if (empprofimagefile.length == 0) {
    $("#snackbarerror").text("Employee Profile Image is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  if (empvoderidfile.length == 0) {
    $("#snackbarerror").text("Employee  ID Proof is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  $("#add_emp").html('Submit <img src="../img/loader.gif">');
  $("#add_emp").attr("disabled", true);

  //post employee data starts here
  var postData = new FormData();
  postData.append("first_name", $('#addempname').val());
  postData.append("username", $('#addempphoneno').val());
  postData.append("email", $('#addempmailid').val());
  postData.append("password", $('#addemppassword').val());
  // postData.append("dob", $('.addempdob').val());
  postData.append("profile_pic", empprofimagefile[0]);
  postData.append("proof_image", empvoderidfile[0]);
  postData.append("userprofile", JSON.stringify({
    "role_id": $('#addemprole').val(),
    "dob": $('.addempdob').val()
  }));
  postData.append("useraddress", JSON.stringify({
    "country": $('#addempcountry').val(),
    "city": $('#addempcity').val(),
    "state": $('#addempstate').val(),
    "area": $('#addemparea').val(),
    "zipcode": $('#addempzipcode').val(),
    "street": $('#addempstreet').val()
  }));
  postData.append("admin_password", $('#addadminpassword').val());


  $.ajax({
    url: addemployees_api,
    type: 'POST',
    data: postData,
    crossDomain: true,
    contentType: false,
    processData: false,
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Authorization", "Token " + localStorage.token);
    },
    success: function(data) {
      $("#add_emp").html('Submit');
      $("#add_emp").attr("disabled", false);
      listemployeedetails(0);
      $(".icon-close.blckclr").click();
      $("#snackbarsuccs").text("Employee Added Successfully!");
      showsuccesstoast();
      $(".cncl").click();
      $('#addempname,#addempmailid,#addempphoneno,.addempdob,#addempstreet,#addemparea,#addempcity,#addempstate,#addempcountry,#addempzipcode,#addemppassword').val("");
      $(".rmvbtn").click();
    },
    error: function(data) {

      $("#add_emp").html('Submit');
      $("#add_emp").attr("disabled", false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();

    }
  })
  //post employee data ends here here
}
//add employees fn ends here


//edit employee details append function starts here
function editdata(me, img) {
  $(".rmvbtn").click();

  var datas = $(me).closest("tr").find('td');
  $("#ed_empnm").val($(datas[2]).find("a").text().trim());
  $("#ed_empph").val($(datas[3]).text());
  $("#ed_empmail").val($(datas[4]).text());
  var mydob = $(datas[5]).text().split('-');
  var dob = mydob[0] + '-' + mydob[1] + '-' + mydob[2];
  $(".ed_empdob").val(dob);
  var myString = $(datas[6]).text().trim();
  if (myString != "N/A") {
    var arr = myString.split(',');
    $("#ed_emparea").val(arr[arr.length - 5]);
    $("#ed_empcity").val(arr[arr.length - 4]);
    $("#ed_empstate").val(arr[arr.length - 3]);
    $("#ed_empcntry").val(arr[arr.length - 2]);
    $("#ed_empzip").val(arr[arr.length - 1]);
    var street = myString.split(',' + arr[arr.length - 5]);
    $("#ed_empst").val(street[0]);
    $('#ed_emprole option[value="' + $(datas[7]).attr("value") + '"]').prop("selected", true);

    var url = img;
    $('.proofpic').empty().append("<img src='" + url + "' height='100%' width='100%'>");
    var url = $($(datas[1]).html()).find('img').attr('src');
    $('.profilepic').empty().append("<img src='" + url + "' height='100%' width='100%'>");
  } else {
    $("#ed_empst,#ed_emparea,#ed_empcity,#ed_empstate,#ed_empcntry,#ed_empzip").val("");
  }
  var url = $($(datas[3]).html()).find('img').attr('src');
  localStorage.emp_id = $(datas[0]).text();
} //edit employee details append function ends here


function editemployee() {

  var postData = new FormData();
  if ($('#ed_empnm').val().trim() == '') {
    $("#ed_empnm").focus();
    $("#snackbarerror").text("Employee name is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  if ($('#ed_empmail').val().trim() == '') {
    $("#ed_empmail").focus();
    $("#snackbarerror").text("Employee Email id is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  var email = $('#ed_empmail').val();
  var atpos = email.indexOf("@");
  var dotpos = email.lastIndexOf(".");
  if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
    $("#ed_empmail").focus();
    $("#snackbarerror").text("Enter a valid Email id");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#ed_empph').val().trim() == '') {
    $("#ed_empph").focus();
    $("#snackbarerror").text("Employee Phone number is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  var currentyear = new Date().getFullYear();
  var usergivenyear = parseInt($('.ed_empdob').val().split("-")[0]);
  if (currentyear - usergivenyear < 18) {
    $("#snackbarerror").text("Your age should be above 18 to register");
    showerrtoast();
    $(".ed_empdob").focus();
    event.stopPropagation();
    return;
  }
  if ($('.ed_empdob').val().trim() == '') {
    $("#snackbarerror").text("Date of Birth is required");
    showerrtoast();
    $(".ed_empdob").focus();
    event.stopPropagation();
    return;
  }
  var currentyear = new Date().getFullYear();
  var usergivenyear = parseInt($('.ed_empdob').val().split("-")[2]);
  if (currentyear - usergivenyear < 18) {
    $("#snackbarerror").text("Your age should be above 18 to register");
    showerrtoast();
    $(".ed_empdob").focus();
    event.stopPropagation();
    return;
  }
  if ($('#ed_empst').val().trim() == '') {
    $("#ed_empst").focus();
    $("#snackbarerror").text("Employee's street is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#ed_emparea').val().trim() == '') {
    $("#ed_emparea").focus();
    $("#snackbarerror").text("Employee's area is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#ed_empcity').val().trim() == '') {
    $("#ed_empcity").focus();
    $("#snackbarerror").text("Employee's city is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#ed_empstate').val().trim() == '') {
    $("#ed_empstate").focus();
    $("#snackbarerror").text("Employee's state is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#ed_empcntry').val().trim() == '') {
    $("#ed_empcntry").focus();
    $("#snackbarerror").text("Employee's Country is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#ed_empzip').val().trim() == '') {
    $("#ed_empzip").focus();
    $("#snackbarerror").text("zipcode is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  if ($('#ed_emprole').val() == 0) {
    $("#snackbarerror").text("Employee's role is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  $(".editempldr").show();
  $(".editempbtn").attr("disabled", true);
  var empprofimagefile = $("#ed_profile")[0].files;
  var empvoderidfile = $("#ed_proof")[0].files;

  // var dateofb = $('.ed_empdob').val().split('-');
  // var datefinal = dateofb[2] + '-' + dateofb[1] + '-' + dateofb[0];
  var user_addr = {
    "country": $('#ed_empcntry').val(),
    "city": $('#ed_empcity').val(),
    "state": $('#ed_empstate').val(),
    "area": $('#ed_emparea').val(),
    "zipcode": $('#ed_empzip').val(),
    "street": $('#ed_empst').val()
  }
  //post employee data starts here
  var postData = new FormData();
  postData.append("first_name", $('#ed_empnm').val());
  postData.append("username", $('#ed_empph').val());
  postData.append("email", $('#ed_empmail').val());
  postData.append("userprofile", JSON.stringify({
    "role_id": $('#ed_emprole').val(),
    "dob": $('.ed_empdob').val()
  }));
  postData.append("useraddress", JSON.stringify(user_addr));
  if (empprofimagefile.length != 0) {
    postData.append("profile_pic", empprofimagefile[0]);
  }
  if (empvoderidfile.length != 0) {
    postData.append("proof_image", empvoderidfile[0]);
  }
  console.log(postData)
  $.ajax({
    url: addemployees_api + (localStorage.emp_id).trim() + "/",
    type: 'PUT',
    data: postData,
    crossDomain: true,
    contentType: false,
    processData: false,
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Authorization", "Token " + localStorage.token);
    },
    success: function(data) {
      $(".editempldr").hide();
      $(".editempbtn").attr("disabled", false);
      listemployeedetails(0);
      $("#snackbarsuccs").text("Employee details Changed Successfully!");
      showsuccesstoast();
      $(".ed_cnl").click();

    },
    error: function(data) {
      $(".editempldr").hide();
      $(".editempbtn").attr("disabled", false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  })
}


//list the role for employee
function loadempdetails() {
  $.ajax({
    url: listemployeesrole_api,
    type: 'GET',
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      employeerole = data
      $('#addemprole,#ed_emprole,#filter_list').empty().append('<option value="0">Role</option>');
      for (var i = 0; i < data.role.length; i++) {
        $('#addemprole,#ed_emprole,#filter_list').append('<option value="' + data.role[i].id + '">' + data.role[i].name + '</option>');
        if(((data.role[i].name).toLowerCase() == "admin") || ((data.role[i].name).toLowerCase() == "users")|| ((data.role[i].name).toLowerCase() == "user")){
          $("#filter_list  option[value='"+data.role[i].id+"']").remove();
        }
      }
    },
    error: function(data) {
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}

// delete employee details starts here
function deactemp(me) {
  var datas = $(me).closest("tr").find('td');
  localStorage.emp_id = $(datas[0]).text();
  $(".showusername").text($(datas[2]).text());
}

//delete admin role final
function enabledisabledelfinal() {
  if ($("#adminpass").val() == "") {
    $("#snackbarerror").text("Admin Password is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  var postData = JSON.stringify({
    "admin_password": $("#adminpass").val()
  })

  $(".enadisdeleteBtn").html('Change <img src="../img/loader.gif">');
  $(".enadisdeleteBtn").attr("disabled", true);

  $.ajax({
    url: addemployees_api + localStorage.emp_id.trim() + '/',
    type: 'DELETE',
    data: postData,
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {

      $(".enadisdeleteBtn").html("Change");
      $(".enadisdeleteBtn").attr("disabled", false);
      listemployeedetails(0);
      $("#adminpass").val("");

    },
    error: function(data) {
      $(".enadisdeleteBtn").html("Change");
      $(".enadisdeleteBtn").attr("disabled", false);

      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();

    }
  }).done(function(dataJson) {

    $("#snackbarsuccs").text("Status Changed Successfully!");
    showsuccesstoast();
    $(".clsecnfrmationmdl").click();

    listemployeedetails(0);

  }); //done fn ends here

} // delete employee details ends here


//view employee profile pic
function viewProof(me, imgproof) {
  var url = imgproof;
  $('.emp_proof').empty().append("<br><img src='" + url + "' height='300px' width='300px'><br><br>");
}

//view employee proof
function viewImg(me) {
  var url = $($(me).html()).find('img').attr('src');
  $('.emp_profile').empty().append("<br><img src='" + url + "' height='300px' width='300px'><br><br>");
}

$('.srchbynm').keyup(function() {
  $('#urlloader').empty();
  $('#urlloader').append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
  listemployeedetails(0);
})


//search employee statrs here
function searchbyname() {
  $(".srch_nm").html('<img src="../img/loader.gif">');
  $(".srch_nm").attr("disabled", true);
  if ($('.srchbynm').val() == "") {
    $("#snackbarerror").text("Enter the name to search");
    showerrtoast();
    $(".srch_nm").html('<i class="icon-action-redo"></i>');
    $(".srch_nm").attr("disabled", false);
    event.stopPropagation();
  } else {
    $('#urlloader').empty().append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
    listemployeedetails(0);
  }
}

function findallfunc() {
  $(".srch_all").html('<img src="../img/loader.gif">');
  $(".srch_all").attr("disabled", true);
  $('.srchbynm').val("");
  $('#urlloader').empty().append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
  listemployeedetails(0);
}


//detail page nav starts here
function gotodetailedpage(id) {
  sessionStorage.employee_id = id;
  window.location.href = "employeedetail.html";
}

//get emp id
function emp_id(id){
  emp_id= id;
}

//employee change passwordset_foremployee_api
function reset_changenewpassword(){
  for(var i = 0;i<2;i++){
    if($(".chg_emp_pass").eq(i).val() == ""){
      $("#snackbarerror").text($(".chnemp_passlb").eq(i).text().replace("*"," ") + " is required");
      showerrtoast();
      event.stopPropagation();
      return;
    }
  }
  $(".channgepassldr").show();
  $(".changepassBtn").attr("disabled",true);
  $.ajax({
    url: addemployees_api + emp_id + "/change_password/",
    type: "PUT",
    data: JSON.stringify({
      "admin_password":$(".chg_emp_pass").eq(0).val(),
      "password":$(".chg_emp_pass").eq(1).val()
    }),
    headers: {
      "Authorization": "Token " + localStorage.token,
      "content-type": 'application/json'
    }
  }).done(function(data){
$(".channgepassldr").hide();
$(".changepassBtn").attr("disabled",false);
$(".closechangepass").click();
$("#newpwd,#renewpass").val("");
    }).fail(function(data){
      $(".channgepassldr").hide();
      $(".changepassBtn").attr("disabled",false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    })
}


//employee results page
$("input[name='view']"). click(function(){
listemployeedetails(0);
})



$("#filter_list").change(function(){
  listemployeedetails(0);
})
