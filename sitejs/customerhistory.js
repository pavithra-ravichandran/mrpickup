// $('.searchcompldr').hide();
var datacustomer = [];

$(function() {
  listcustomerdetails(0);
});

function listcustomerdetails(type) {
  var url;
  (type == 0) ? (url = customerhistory_api) : url = type;
  var postData = JSON.stringify({
    "search": $('.srchbynm').val()
  })

  $.ajax({
    url: url,
    type: 'POST',
    data: postData,
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      $('.loadempdetails').empty();
      $('.findalldr').hide();
      $(".searchcompldr").html('<i class="icon-action-redo"></i>');
      $(".searchcompldr,.searchcompldr_all").attr("disabled", false);
      $(".listemp,.loadempdetails").empty()
      datacustomer = data;
      if (data.results.length == 0) {
        $("#tablehead").hide();
        $(".listemp").empty().prepend('<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>');

      } else {
        $("#tablehead").show();
        var page = url.split("=");
        for (var i = 0; i < data.results.length; i++) {
          // var order = [];
          // order = JSON.stringify(data.results[i].orders);

          $('.loadempdetails').append(`<tr class="empid" value="${data.results[i].id}">
                                                                <td><center>${i+1 *parseInt((page[1] == undefined) ? "1" : page[1] )} </center></td>
                                                                <td><center>
                                                                   ${data.results[i].first_name}</center>
                                                                </td>
                                                                <td><center> ${data.results[i].username}</center></td>
                                                                <td><center> ${data.results[i].email}</center></td>
                                                                <td><center>
                                                                    <a onclick="customerdetailpage(${data.results[i].id})" class="btn btn-circle btn-icon-only fsearchrbtn"><i class="icon-info icnsend"></i></a>
                                                                </center></td>

                                                            </tr>`);
        }

        if (data.next_url != null) {
          var next1 = (data.next_url).split('=');
          var val = data.count / data.page_size;
          if (val % 1 === 0) {
            //if number is integer
            var val = val;
          } else {
            var val = parseInt(val) + 1;
          }
          var obj = $('#pagination').twbsPagination({
            totalPages: val,
            visiblePages: 5,
            onPageClick: function(event, page) {
              console.info(page);
              listcustomerdetails(next1[0] + "=" + (page));
            }
          });
        }

      }
    },
    error: function(data) {
      console.log("Error Occured in dynamic suadmin loading");

    }
  });
}

//function for customer detailed page
function customerdetailpage(id) {
  sessionStorage.customer_id = id;
  window.location.href = "customerdetail.html";
}



//old code
var value;

function loadorders(key) {
  // console.log(obj);
  console.log(datacustomer)
  var obj = datacustomer;
  if (obj[key]) {
    value = obj[key];
    console.log(value);
    $('.listcustomer_details').empty();
    // $('#csstablecomp').empty();
    if (value.orders.length == 0) {
      // $('#csstablecomp').hide();
      $('.tableheadcust').hide();
      $('.listcustomer_details').append(`<center><p>No History Found</p></center>`);
    } else {
      $('.tableheadcust').show();
      for (var i = 0; i < value.orders.length; i++) {
        var procost = (value.orders[i].orderdetails.product_cost != null) ? value.orders[i].orderdetails.product_cost : 0;
        var waitcost = (value.orders[i].orderdetails.waiting_cost != null) ? value.orders[i].orderdetails.waiting_cost : 0;
        var pickuptype = (value.orders[i].pickup_type) ? '../img/pick.svg' : '../img/buy.svg';
        $('.listcustomer_details').append(`   <tr>
                                                        <td><center>${i+1}</center></td>
                                                        <td><center>${value.orders[i].id}</center></td>
                                                        <td><center>${value.orders[i].orderdetails.approx_dist}</center></td>
                                                        <td><center>Rs.${value.orders[i].orderdetails.delivery_cost + procost + waitcost}</center></td>
                                                        <td><center><img src="${pickuptype}" class="width20"></center></td>
                                                        <td><center>${value.orders[i].orderdetails.status.name}</center></td>
                                                    </tr>`);
      } //for loop ends here
    } //else ends here
  }



}

//function for search starts here
function searchbynamecomp() {
  $('#urlloader').empty();
  $('#urlloader').append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
  $(".searchcompldr").html('<img src="../img/loader.gif">');
  $(".searchcompldr").attr("disabled", true);
  listcustomerdetails(1);
}

//function for find all
function findallcustomer() {
  $('#urlloader').empty();
  $('#urlloader').append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
  $('.srchbynm').val('');
  $('.findalldr').show();
  $(".searchcompldr_all").attr("disabled", true);
  $('.srchbynm').val('')
  listcustomerdetails(0);
}


$('.srchbynm').keyup(function() {
  $('#urlloader').empty();
  $('#urlloader').append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
  listcustomerdetails(0);
})
