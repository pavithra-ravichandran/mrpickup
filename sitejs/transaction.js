$(function() {
loadtransaction(0);
});

//function to load transaction starts here
function loadtransaction(type) {
  var url;
  (type == 0) ? url = rateclearance_api + sessionStorage.employee_id + "/user_history/" : url = type;
  var postData = JSON.stringify({
	"search":$("#search_query").val(),
  "from_date":$("#date_picker").val(),
  "to_date":$("#date_picker1").val()
  });

  $.ajax({
    url: url,
    type: "POST",
    data: postData,
    headers: {
      "content-type": "application/json",
      "Authorization": "token " + localStorage.token
    }
  }).done(function(data) {
    $(".starzappend").empty();
    $(".findbtnall").attr("disabled",false);
    $(".findalldr").hide();
    (data.employee_deatils.userprofile.profile_pic != null) ? $(".profile_img").attr("src", data.employee_deatils.userprofile.profile_pic): "";
    var datejoined = data.employee_deatils.date_joined.split('T')
    var content = datejoined[0].split('-');
    var datejoin = content[2] + " " + month[content[1] - 1] + " " + content[0];
    $(".employeename").text(data.employee_deatils.first_name);
    $(".join_date").text(datejoin);
    var star = data.employee_deatils.userprofile.rating;
    if (star) {
      var whole = Math.floor(star);
      var high = 5 - Math.ceil(star);
      var dec = (star * 10) % 10;
      for (var i = 0; i < whole; i++) {
        $(".starzappend").append(`
                     <i class="fa fa-star purplecolor" aria-hidden="true"></i>
                 `);
      }
      if (dec > 4) {
        $(".starzappend").append(`
                    <i class="fa fa-star-half-o purplecolor" aria-hidden="true"></i>`);
      } else if (dec < 5 && dec != 0) {
        $(".starzappend").append(`
                    <i class="fa fa-star-o purplecolor" aria-hidden="true"></i>
                    `);
      }
      for (var i = 0; i < high; i++) {
        $(".starzappend").append(`
                    <i class="fa fa-star-o purplecolor" aria-hidden="true"></i>
                      `);
      }
    } else {
      for (var i = 0; i < 5; i++) {
        $(".starzappend").append(`
                    <i class="fa fa-star-o purplecolor" aria-hidden="true"></i>
                      `);
      }
    }

    ((data.results.length == 0) ||(parseInt(data.results[0].total_amount)-parseInt(data.results[0].paid_amount)) == 0) ?$(".due_amount").text("Rs. 0") :  $(".due_amount").text("Rs. " + (parseInt(data.results[0].total_amount)-parseInt(data.results[0].paid_amount)));
      $(".employeereportclass").empty();
    // {
    //        "id": 4,
    //        "employee": {
    //            "id": 15,
    //            "first_name": "suriya",
    //            "last_name": "",
    //            "username": "85236974",
    //            "role": "Delivery Boy"
    //        },
    //        "total_amount": 80,
    //        "paid_amount": 50,
    //        "description": "what happen mari",
    //        "created_on": "2018-01-20T15:50:02.764696",
    //        "created_by": {
    //            "id": 1,
    //            "first_name": "",
    //            "last_name": "",
    //            "username": "Billiontags",
    //            "role": "Admin"
    //        }
    //    }
    if(data.results.length != 0 ){
    for(var i= 0;i<data.results.length;i++){
      var datejoined = data.results[i].created_on.split('T')
      var content = datejoined[0].split('-');
      var datejoin = content[2] + " " + month[content[1] - 1] + " " + content[0];
      $(".employeereportclass").append(`<tr>
        <td> ${i+1} </td>
        <td> Rs.${data.results[i].total_amount} </td>
        <td> Rs.${data.results[i].paid_amount} </td>
        <td> Rs.${parseInt(data.results[i].total_amount)- parseInt(data.results[i].paid_amount)} </td>
        <td> ${datejoin} </td>
        <td> ${data.results[i].description} </td>
        <td> ${data.results[i].created_by.first_name +'/'+data.results[i].created_by.role}</td>
      </tr>`)
    }
  }else{
    $(".empthead").hide();
    $(".imgemptyappend").empty().append('<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>');
  }

    if (data.next_url != null) {
      var next1 = (data.next_url).split('=');
      var val = data.count / data.page_size;
      if (val% 1 === 0) {
        //if number is integer
        var val = val;
      } else {
        var val = parseInt(val) + 1;
      }
      var obj = $('#pagination').twbsPagination({
        totalPages: val,
        visiblePages: 5,
        onPageClick: function(event, page) {
          console.info(page);
          loadtransaction(next1[0] + "=" + (page));
        }
      });
    }

  }).fail(function(data) {
    for (var key in JSON.parse(data.responseText)) {
      $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
    }
    showerrtoast();
  })
}
//function to load transaction ends here


//find function starts here
function find(){
  $('#urlloader').empty();
  $('#urlloader').append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
  loadtransaction(0);
}
//findall function starts here
function findall(){
  $('#urlloader').empty();
  $('#urlloader').append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
  $(".findbtnall").attr("disabled",true);
  $(".findalldr").show();
  $("#date_picker,#date_picker1").val("");
  loadtransaction(0);
}

//search on keyup starts here
$("#search_query").keyup(function(){
  find();
})
