$(function() {
  loadunconformed_orders(0);
});

//load funcion starts here
function loadunconformed_orders(type) {
  var postData = JSON.stringify({
    "status": 1,
    "start_date": $("#date_picker").val(),
    "end_date": $("#date_picker1").val(),
    "search": $("#search_query").val()
  });
  sessionStorage.order_type = 0;
  sessionStorage.order_page = type;


  if (type == 0) {
    var url = listorder_api;
    $('#urlloader1').empty().append(`<ul class="list-inline pagination floatright" id="pagination1"></ul>`);
  } else {
    var url = type;
  }

  $.ajax({
    url: url,
    data: postData,
    type: "POST",
    headers: {
      "content-type": "application/JSON",
      "Authorization": "token " + localStorage.token
    }
  }).done(function(data) {
    var page_no = 1,
      page_get;
    if (data.next_url != null) {
      var page_get = (data.next_url).split('=');
      var page_no = parseInt(page_get[1]) - 1;
    } else if (data.prev_url != null) {
      var page_get = (data.prev_url).split('=');
      var page_no = parseInt(page_get[1]) + 1;
    } else if ((data.next_url == null) && (data.prev_url == null)) {
      var page_no = 1,
        page_get;
    }
    $(".findalldr").hide();
    $(".gobtnall").attr("disabled", false);
    $(".unconformedlist,.imgforunconformedodrs").empty();
    if (data.results.length != 0) {
      $(".unconformedclass").show();
      for (var i = 0; i < data.results.length; i++) {
        var image;
        (data.results[i].pickup_image == null || data.results[i].pickup_image.length == 0) ? image = '../img/noimage.png': image = data.results[i].pickup_image[0].image;

        var created_on = data.results[i].created_on;
        var newdate = created_on.split('T');
        var now = newdate[0].split('-');
        var timeval = Convert_time(newdate[1].slice(0, 8));
        var datenew = now[2] + '/' + now[1] + '/' + now[0];
        if (data.results[i].pickup_time != null) {
          var expected_on = data.results[i].pickup_time;
          var newdate = expected_on.split('T');
          var now = newdate[0].split('-');
          var timeexp_val = Convert_time(newdate[1].slice(0, 8));
          var expect_on = now[2] + '/' + now[1] + '/' + now[0];
          var expected_dateon = expect_on + " " + timeexp_val;
        } else {
          var expected_dateon = "Not Mentioned";
        }
        $(".unconformedlist").append(`<tr class="appendreadclass${data.results[i].id}"><td>${(page_no == 1)?(page_no) * (i + 1) : (page_no * data.page_size)+ (i + 1)}</td><td>  ${data.results[i].order_id}</td><td onclick="viewproduct(this,${data.results[i].id},0)"><center><img src="${image}" class="img-responsive classborderedimg cptr" href="#viewfullimg" data-toggle="modal"></center></td><td>${data.results[i].pickup_details[0].name}</td><td>${datenew} ${timeval}</td><td>${expected_dateon}</td><td><img src="../img/buy.svg" class="width50"></td><td>  <a  onclick="loadaddress('${data.results[i].id}','${data.results[i].pickup_type}',0);" class="asntsk">More</a></td></tr>`);
        (data.results[i].is_read)?"":$(".appendreadclass"+data.results[i].id).addClass("readclass_order");

      }
      // <td>${data.results[i].category_name}</td>
      // <td><a onclick="getuserid(${data.results[i].id})" class="asntsk">click here</a></td>
      if ((data.next_url != null) || (data.prev_url != null)) {
        $('#urlloader1').show();
        if (data.next_url != null) {
          var next1 = (data.next_url).split('=');
        } else if (data.prev_url != null) {
          var next1 = (data.prev_url).split('=');
        }
        var val = data.count / data.page_size;
        if (val % 1 === 0) {
          //if number is integer
          var val = val;
        } else {
          var val = parseInt(val) + 1;
        }
        var obj = $('#pagination1').twbsPagination({
          totalPages: val,
          visiblePages: 5,
          onPageClick: function(event, page) {
            console.info(page);
            loadunconformed_orders(next1[0] + "=" + (page));
          }
        });
      } else {
        $('#urlloader1').hide();
      }
    } else {
      $(".unconformedclass").hide();
      $('.imgforunconformedodrs').empty().append(`<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>`);
    }
  }).fail(function(data) {
    $(".findalldr").hide();
    $(".gobtnall").attr("disabled", false);
    for (var key in JSON.parse(data.responseText)) {
      $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
    }
    showerrtoast();
  })
}

//send amount conformed
function sendamt() {

  if ($("#estcost").val() == "") {
    $("#snackbarerror").text("Enter the cost to submit");
    showerrtoast();
    event.stopPropagation();
    return;
  }
  $(".addcostldr").show();
  $(".addcostbtn").attr("disabled", true);
  var postData = JSON.stringify({
    "product_cost": $("#estcost").val()
  })
  $.ajax({
    url: listproductimg_api + user_id + "/product_cost/",
    data: postData,
    type: "POST",
    headers: {
      "content-type": "application/JSON",
      "Authorization": "token " + localStorage.token
    }
  }).done(function(data) {
    $(".closeadmodl").click();
    $("#estcost").val("");
    $(".addcostldr").hide();
    $(".addcostbtn").attr("disabled", false);
    loadunconformed_orders(0);
    $(".clsecnfrmationmdl").click();
  }).fail(function(data) {
    $(".addcostldr").hide();
    $(".addcostbtn").attr("disabled", false);
    for (var key in JSON.parse(data.responseText)) {
      $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
    }
    showerrtoast();
  })
}


//call for 2 sec
setInterval(function(){
if(sessionStorage.order_type != undefined){

    switch (parseInt(sessionStorage.order_type)) {
      case 0:
      if(sessionStorage.order_page == sessionStorage.order_type){
        loadunconformed_orders(0);
      }else{
        loadunconformed_orders(sessionStorage.order_page );
      }
        break;
      case 1:
      if(sessionStorage.order_page == sessionStorage.order_type){
        listorders(0);
      }else{
        listorders(sessionStorage.order_page);
      }
        break;
      case 2:
      if(sessionStorage.order_page == sessionStorage.order_type){
        listpendingorders(0);
      }else{
        listpendingorders(sessionStorage.order_page);
      }
        break;
      case 3:
      if(sessionStorage.order_page == sessionStorage.order_type){
        listcompletedorders(0);
      }else{
        listcompletedorders(sessionStorage.order_page);
      }
        break;
      case 4:
      if(sessionStorage.order_page == sessionStorage.order_type){
        listcancelledorders(0);
      }else{
        listcancelledorders(sessionStorage.order_page);
      }
        break;
    }
}

},(6000*10*5));
