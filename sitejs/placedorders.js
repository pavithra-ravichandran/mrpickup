$(".showforplaced,.saveclass_btn,.deleteconfirmation").hide();
var product_details = [],
  deleteditem = [],
  deleteitem_div, classlength = 0;
loadcategory();
loadunits();
//function to load data starts here
function listorders(type) {
  sessionStorage.order_type = 1;
  sessionStorage.order_page = type;

  if (type == 0) {
    var url = listorder_api;
    $('#urlloader2').empty().append(`<ul class="list-inline pagination floatright" id="pagination2"></ul>`);
  } else {
    var url = type;
  }

  var postData = JSON.stringify({
    "status": 2,
    "start_date": $("#date_picker").val(),
    "end_date": $("#date_picker1").val(),
    "search": $("#search_query").val()
  });
  $.ajax({
    url: url,
    type: 'POST',
    data: postData,
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      var page_no = 1,
        page_get;
      if (data.next_url != null) {
        var page_get = (data.next_url).split('=');
        var page_no = parseInt(page_get[1]) - 1;
      } else if (data.prev_url != null) {
        var page_get = (data.prev_url).split('=');
        var page_no = parseInt(page_get[1]) + 1;
      } else if ((data.next_url == null) && (data.prev_url == null)) {
        var page_no = 1,
          page_get;
      }

      $(".placedoredrsimg,.addplacedorders").empty();
      $(".findalldr").hide();
      $(".gobtnall").attr("disabled", false);
      if (data.results.length == 0) {
        $(".placedclass").hide();
        $(".placedoredrsimg").empty().append('<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>');
      } else {
        $(".placedclass").show();
        for (var i = 0; i < data.results.length; i++) {
          if (data.results[i].pickup_image == null || data.results[i].pickup_image.length == 0) {
            var image = '../img/noimage.png';
          } else {
            var image = data.results[i].pickup_image[0].image;
          }
          var created_on = data.results[i].created_on;
          var newdate = created_on.split('T');
          var now = newdate[0].split('-');
          var timeval = Convert_time(newdate[1].slice(0, 8));
          var datenew = now[2] + '/' + now[1] + '/' + now[0];
          if (data.results[i].pickup_time != null) {
            var expected_on = data.results[i].pickup_time;
            var newdate = expected_on.split('T');
            var now = newdate[0].split('-');
            var timeexp_val = Convert_time(newdate[1].slice(0, 8));
            var expect_on = now[2] + '/' + now[1] + '/' + now[0];
            var expected_dateon = expect_on + " " + timeexp_val;
          } else {
            var expected_dateon = "Not Mentioned";
          }
          var product;
          (data.results[i].pickup_type) ? product = '../img/pick.svg': product = '../img/buy.svg';

          $(".addplacedorders").append(`<tr class="appendreadclass${data.results[i].id}"><td>${(page_no == 1) ? (page_no) * (i + 1) : (page_no * data.page_size) + (i + 1)}</td><td>  ${data.results[i].order_id}</td><td onclick="viewproduct(this,${data.results[i].id},0)"><center><img src="${image}" class="img-responsive classborderedimg cptr" href="#viewfullimg" data-toggle="modal"></center></td><td>${data.results[i].pickup_name}</td><td>${datenew} ${timeval}</td><td>${expected_dateon}</td><td><img src=${product} class="width50"></td><td>  <a  onclick="loadaddress('${data.results[i].id}','${data.results[i].pickup_type}',1);" class="asntsk">More</a></td><td><a onclick="listnearestemployee(${data.results[i].id},0)" class="asntsk">click here</a></td></tr>`);

          (data.results[i].is_read) ? "" : $(".appendreadclass" + data.results[i].id).addClass("readclass_order");
        }
      }
      // <td>${data.results[i].category_name}</td>
      if ((data.next_url != null) || (data.prev_url != null)) {
        $("#urlloader2").show();
        if (data.next_url != null) {
          var next1 = (data.next_url).split('=');
        } else if (data.prev_url != null) {
          var next1 = (data.prev_url).split('=');
        }
        var val = data.count / data.page_size;
        if (val % 1 === 0) {
          var val = val;
        } else {
          var val = parseInt(val) + 1;
        }
        var obj = $('#pagination2').twbsPagination({
          totalPages: val,
          visiblePages: 5,
          onPageClick: function(event, page) {
            console.info(page);
            listorders(next1[0] + "=" + (page));
          }
        });
      } else {
        $("#urlloader2").hide();
      }
    },
    error: function(data) {
      $(".findalldr").hide();
      $(".gobtnall").attr("disabled", false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}

//list the nearest employee on click assign
function listnearestemployee(datam, type_val) {
  $('.listemployee').empty();
  placerdenied = type_val;
  localStorage.empid = datam;
  $.ajax({
    url: listemployeenearest_api + datam + '/',
    type: 'get',
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      $(".rejected_details").empty();
      if (data.declined_employees.length != 0) {
        $(".show_rejectemmpshow").show();
        for (var i = 0; i < data.declined_employees.length; i++) {
          $(".rejected_details").append(`<div class="row">
                  <div class="col-lg-4 col-md-4">
                     <b class="aprxdet">${data.declined_employees[i].first_name}</b>
                  </div>
                  <div class="col-lg-8 col-md-8">
                      <span class="emploe_name">${data.declined_employees[i].cancel_reason}</span>
                  </div>
           </div>`);
        }
      } else {
        $(".show_rejectemmpshow").hide();
      }
      $('.listemployeee').empty();
      if (data.length != 0) {
        $(".assignempthead").show();
        for (var i = 0; i < data.employees.length; i++) {
          $('.listemployeee').append(`<tr><td> ${data.employees[i].first_name + " " + data.employees[i].last_name}</td><td> ${data.employees[i].id} </td><td> ${data.employees[i].username} </td><td>${data.employees[i].distance} Kms</td><td>Rs  ${(data.employees[i].due_amount).toFixed(2)}</td><td><a onclick='clickassignanother(${data.employees[i].id}, ${localStorage.empid})' class='asntsk empaddBtn${data.employees[i].id}'>Assign<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr editofferldr${data.employees[i].id}" style="display: none;"></i></a></td></tr>`);
        }
      } else {
        $(".assignempthead").hide();
        $('.listemployeee').append(`<center><strong>No Employees To List</center></strong>`);
      }
      $("#asngtsk").modal();
    },
    error: function(data) {

      $(".subadmintable").hide();
      $(".tablestart").text('No Data Found').css('color', '#e43a45')
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}


//assign employee on assign function
function clickassignanother(data, id) {
  $(".empaddBtn" + id).show();
  $(".empaddBtn" + id).attr("disabled", false);

  var postData = JSON.stringify({
    "order": id,
    "assign_employee": data
  });

  $.ajax({
    url: assignemployee_api,
    type: 'post',
    data: postData,
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {

      $(".empaddBtn" + id).hide();
      $(".empaddBtn" + id).attr("disabled", false);
      $(".closeadd_empmodal").click();
      $("#snackbarsuccs").text("Employee assigned Successfully!");
      showsuccesstoast();
      (placerdenied == 0) ? listorders(0): requestcancelled();
    },
    error: function(data) {

      $(".empaddBtn" + id).hide();
      $(".empaddBtn" + id).attr("disabled", false);

      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  })

}
//ends here

//function to convert time
function Convert_time(time) {
  time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
  if (time.length > 1) {
    time = time.slice(1);
    time[3] = +time[0] < 12 ? 'AM' : 'PM';
    time[0] = +time[0] % 12 || 12;
  }
  return time.join('');
}

//function to append more details

function loadaddress(id, picktype, type) {
  $(".saveclass_btn").hide();

  user_id = id;
  $.ajax({
    url: singleorder_api + id + '/',
    type: 'get',
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      (type == 0) ? ($(".appendheaderclass").text("Unconfirmed Orders Information") && (loadunconformed_orders(0))) : ($(".appendheaderclass").text("Placed Orders Information") && (listorders(0)));
      //       {
      //     "id": 3,
      //     "pickup_name": "Jegatheeswaran",
      //     "pickup_loc": "477, Model House Rd, CIT Nagar West, CIT Nagar, Chennai, Tamil Nadu 600035, India",
      //     "pickup_mob": "9715868154",
      //     "del_name": "Krishna",
      //     "del_loc": "Nivedha Apartments, West Rd, CIT Nagar West, West Saidapet, Chennai, Tamil Nadu 600035, India",
      //     "del_mob": "9715868154",
      //     "pickup_type": true,
      //     "pickup_time": "2017-07-01T12:33:55",
      //     "created_on": "2017-11-23T16:22:09.447137",
      //     "orderdetails": {
      //         "id": 3,
      //         "approx_dist": 1.635,
      //         "time_taken": "00:07:05",
      //         "delivery_cost": 80,
      //         "product_cost": null,
      //         "waiting_time": null,
      //         "waiting_cost": null,
      //         "status": {
      //             "id": 1,
      //             "name": "Order Placed"
      //         },
      //         "delivery_time": null
      //     },
      //     "orderassignment": null
      // categories:[{category: "Sample2", products: [{name: "djghfjd"}, {name: "jfg"}]}]
      // "store_code":null
      // "store_name":null
      // "descriptions":null
      // product_name:[{id: 30, name: "food", category: "Sample 3"},
      // {id: 30, name: "food1", category: "Sample 3"},
      // {id: 30, name: "food2", category: "Sample 3"}]
      // }
      // [{id: 2, name: "Kilogram"}, {id: 3, name: "Litre"}, {id: 1, name: "None"}, {id: 4, name: "Quantity"}]

      $.each(data.categories, function(key, val) {
        classlength += val.products.length;
      })
      $('.deleteconfirmation').hide();
      $("#appendedit_content").empty();
      for (var i = 0; i < data.categories.length; i++) {
        for (var j = 0; j < data.categories[i].products.length; j++) {
          $("#appendedit_content").append(`<div class="row classproducatdetails" value="${data.categories[i].products[j].id}">
              <div class="form-group form-md-line-input  has-info is-empty ptop0 mb20"> <div class="col-md-1"></div>
            <div class="col-md-3"><select onchange="change_func()" class="form-control ipclkaddemp edited roleclr addcatgyreplica categoryselect changefield" id="addcatgy${i}${j}">${categories}</select></div>
            <div class="col-md-2"><input onkeyup="change_func()" type="text"  class="form-control input-sm ipclkcpwd nameenter changefield" name="" value=${data.categories[i].products[j].name} style="height:34px"></div>
            <div class="col-md-2"><input onkeyup="change_func()" type="text"  class="form-control input-sm ipclkcpwd quantityenter changefield" name="" value=${data.categories[i].products[j].quantity} style="height:34px"></div>
            <div class="col-md-3"><select onchange="change_func()" class="form-control ipclkaddemp edited roleclr addcatgyreplica unitselect changefield" id="editcategy${i}${j}">${quantities}</select></div>
            <div class="col-md-1"><a class="btn btn-circle btn-icon-only fsearchrbtn subaddprod" onclick="replicatesub(this);"><i class="fa ${($(".classproducatdetails").length == classlength-1) ? "fa-plus" :"fa-trash-o" }" aria-hidden="true"></i></a></div></div></div>  `);
          $("#editcategy" + i + j).val(data.categories[i].products[j].units);
          $("#addcatgy" + i + j).val(data.categories[i].category.id);
        }
      }

      // $(".product_details").empty();
      // var units = ["None", "Kg", "Ltr", "Qty"];
      // for (var i = 0; i < data.categories.length; i++) {
      //   $(".product_details").append(`<b class="aprxdet">${data.categories[i].category}</b> <span class="appdcategory${i}"></span><br>`);
      //   for (var j = 0; j < data.categories[i].products.length; j++) {
      //     if (j != (data.categories[i].products.length - 1)) {
      //       $(".appdcategory" + i).append(`${data.categories[i].products[j].name +"-" +data.categories[i].products[j].quantity + " " + units[parseInt(data.categories[i].products[j].units) - 1] },`);
      //     } else {
      //       $(".appdcategory" + i).append(`${data.categories[i].products[j].name +"-" +data.categories[i].products[j].quantity + " " + units[parseInt(data.categories[i].products[j].units) - 1] }`);
      //     }
      //   }
      // }
      ((data.store_name == null) || (data.store_name == "")) ? $(".store_name").text("Not Mentioned"): $(".store_name").text(data.store_name);
      ((data.store_code == null) || (data.store_code == "")) ? $(".store_id").text("Not Mentioned"): $(".store_id").text(data.store_code);
      ((data.descriptions == null) || (data.descriptions == "")) ? $(".description_note").text("Not Mentioned"): $(".description_note").text(data.descriptions);
      (data.orderdetails.product_cost == "0.00") ? $(".product_costshow").hide(): $(".product_costshow").show() && $(".amtproduct_cost").text("Rs. " + data.orderdetails.product_cost);
      classlength = 0;

      $(".order_idno").text(data.order_id);

      var pickty = (picktype == 'true') ? 'Pick & Deliver' : 'Buy & Deliver';
      $('.delcharge').text(" " + data.orderdetails.delivery_cost);
      $('.paytype').text(" " + data.payment_type);
      $('.deliverytype').text(" " + data.delivery_type);
      $('.picktype').text("  " + pickty);
      $('.pickupname').text(" " + data.pickup_name);
      $('.pickuploc').text(" " + data.pickup_loc);
      $('.pickuphno').text(" " + data.pickup_mob);
      $('.deliveryname').text(" " + data.del_name);
      $('.deliveryaddr').text(" " + data.del_loc);
      $('.deliverynum').text(" " + data.del_mob);

      var time_taken = (data.orderdetails.time_taken).split(":");
      if (time_taken[0] != "00") {
        var time_is = data.orderdetails.time_taken;
        $('.timeexp').text(time_is + " Hours");
      } else if (time_taken[1] != "00") {
        var time_is = data.orderdetails.time_taken.slice(3);
        $('.timeexp').text(time_is + " Mins");
      } else {
        $('.timeexp').text('0 Min');
      }
      (type == 0) ? $(".showforplaced").show(): $(".showforplaced").hide();
      $("#viewmoremodalplaced").modal();
    },
    error: function(data) {
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  });
}


//replicate function starts here
function replicatesub(me) {
  if ($(me).parent().children().children().hasClass("fa-plus")) {
    if ($(me).parent().parent().children().find(".addcatgyreplica").val() == 0 || $(me).parent().parent().children().find(".nameenter").val() == "" || $(me).parent().parent().children().find(".quantityenter").val() == "" ||
      $(me).parent().parent().children().find(".addcatgyreplica").val() == 0) {
      $("#snackbarerror").text("Enter the Product details to add another product");
      showiperrtoast();
      event.stopPropagation();
      return;
    }
    $(".subaddprod").empty().append(`<i class="fa fa-trash-o" aria-hidden="true"></i>`);
    $("#appendedit_content").append(`<div class="row classproducatdetails" value="">
        <div class="form-group form-md-line-input  has-info is-empty ptop0 mb20"> <div class="col-md-1"></div>
      <div class="col-md-3"><select class="form-control ipclkaddemp edited roleclr addcatgyreplica categoryselect changefield" onchange="change_func()">${categories}</select></div>
      <div class="col-md-2"><input type="text" onkeyup="change_func()" class="form-control input-sm ipclkcpwd nameenter changefield" name="" value=""  style="height:34px"></div>
      <div class="col-md-2"><input type="text" onkeyup="change_func()" class="form-control input-sm ipclkcpwd quantityenter changefield" name="" value=""  style="height:34px"></div>
      <div class="col-md-3"><select onchange="change_func()" class="form-control ipclkaddemp edited roleclr addcatgyreplica unitselect changefield">${quantities}</select></div>
      <div class="col-md-1"><a class="btn btn-circle btn-icon-only fsearchrbtn subaddprod" onclick="replicatesub(this);"><i class="fa fa-plus" aria-hidden="true"></i></a></div></div></div>  `);
  } else {
    deleteitem_div = $(me).parent().parent()
    $('.deleteconfirmation').slideToggle();
  }
}

function deleteimage() {
  deleteditem.push(Number(deleteitem_div.parent().attr("value")));
  deleteitem_div.parent().remove()
  $('.deleteconfirmation').slideToggle();
  save_changes();
}

//save changes functionality starts here
function save_changes() {
  for (i = 0; i < $(".classproducatdetails").length; i++) {
    if (($(".categoryselect").eq(i).val() != 0) && ($('.unitselect').eq(i).val() != 0) && ($('.nameenter').eq(i).val() != "") && ($('.quantityenter').eq(i).val() != "")) {
      product_details.push({
        "id": $(".classproducatdetails").eq(i).attr("value"),
        "category_id": $(".categoryselect").eq(i).val(),
        "name": $(".nameenter").eq(i).val(),
        "quantity": $(".quantityenter").eq(i).val(),
        "units_id": $(".unitselect").eq(i).val()
      })
    } else {

    }
  }

  $(".savechanges_ldr").show();
  var postData = JSON.stringify({
    "products": product_details,
    "deleted_product": deleteditem
  });
  $.ajax({
    url: singleorder_api + user_id + "/edit_product/",
    type: 'PUT',
    data: postData,
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      $(".savechanges_ldr,.saveclass_btn").hide();
      deleteditem = [];
      product_details = [];
      deleteitem_div = "";
      $("#snackbarsuccs").text("Changes Saved");
      showsuccesstoast();
    },
    failure: function(data) {
      $(".savechanges_ldr").hide();
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  })
}


// on input change shoe div
function change_func() {
  $(".saveclass_btn").show();
}


//load categories dropdown
var categories = '',
  quantities = '';

function loadcategory() {
  $.ajax({
    url: listemployeesrole_api,
    type: 'get',
    headers: {
      "content-type": 'application/json'
      // "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      $('.categoryselect').empty();
      for (var i = 0; i < data.category.length; i++) {
        // $('#addcatgy').append('<option value=' + data.category[i].id + '>' + data.category[i].name + '</option>');
        $('.categoryselect').append('<option value=' + data.category[i].id + '>' + data.category[i].name + '</option>');
        categories += '<option value=' + data.category[i].id + '>' + data.category[i].name + '</option>';
      }
    },

    error: function(data) {
      console.log("error occured during category load");
    }
  });
}

function loadunits() {
  $.ajax({
    url: unitsapi,
    type: "GET",
    headers: {
      "content-type": 'application/json'
      // "Authorization": "Token " + localStorage.token
    },
    success: function(data) {
      $(".unitselect").empty();
      for (var i = 0; i < data.length; i++) {
        // $('#addcatgy').append('<option value=' + data.category[i].id + '>' + data.category[i].name + '</option>');
        $('.unitselect').append('<option value=' + data[i].id + '>' + data[i].name + '</option>');
        quantities += '<option value=' + data[i].id + '>' + data[i].name + '</option>';
      }
    },
    error: function(data) {
      console.log("error occured during category load");
    }
  })
}
