$(function() {
  $(".nav.navbar-nav.pull-right").append(`<li class="separator hide"> </li>  <li class="dropdown dropdown-user dropdown-dark" id="header_task_bar">
    <a onclick="gotOrderTab(0);" class="dropdown-toggle">
                       Unconformed
                       <span class="badge badge-primary"> 0 </span>
                   </a>

  </li>
  <li class="dropdown dropdown-user dropdown-dark" id="header_task_bar">
    <a onclick="gotOrderTab(1);" class="dropdown-toggle">
                          Placed
                          <span class="badge badge-primary"> 0 </span>
                      </a>

  </li>
  <li class="dropdown dropdown-user dropdown-dark" id="header_task_bar">
    <a  onclick="gotOrderTab(2);"  class="dropdown-toggle">
                             Pending
                             <span class="badge badge-primary"> 0 </span>
                         </a>

  </li>
  <li class="dropdown dropdown-user dropdown-dark" id="header_task_bar">
    <a  onclick="gotOrderTab(3);"  class="dropdown-toggle">
                                Completed
                                <span class="badge badge-primary"> 0 </span>
                            </a>

  </li>
  <li class="dropdown dropdown-user dropdown-dark">
      <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <span class="username username-hide-on-mobile whiteclr dynusername"> Admin </span>
                        <img alt="" class="img-circle dynuserprof" src="../img/logo2.png" /> </a>
      <ul class="dropdown-menu dropdown-menu-default">
        <li>
          <a href="#changepassmodal" data-toggle="modal" class="blckclr">
                                <i class="icon-lock themeclrfrst"></i> Change Password </a>
        </li>
        <li>
          <a onclick="logout()" class="blckclr">
                                <i class="icon-key themeclrfrst"></i> Log Out </a>
        </li>
      </ul>
    </li>`);
  if (localStorage.token != undefined) {
    loadheader();
  }
});

function gotOrderTab(id){
	if(window.location.pathname.indexOf("order-status.html") > -1){
		$(".nav-tabs a").eq(id).click();
	} else {
		window.location.href = "order-status.html#tab_15_" + id;
	}
}

//function to load header
function loadheader() {
  $.ajax({
    url: notification_api,
    type: 'get',
    headers: {
      "content-type": 'application/json',
      "Authorization": "Token " + localStorage.token
    },
    success: function(data) {

      var array = ["unconformed","placed","pending","completed"];
      for (var i = 0; i < 4; i++) {
        $(".badge-primary").eq(i).text(data[array[i]]);
      }
    },
    error: function(data) {
      if (data.status == 401) {
        clearInterval(notifyInterval);
        localStorage.clear();
      } else {
        for (var key in JSON.parse(data.responseText)) {
          $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
      }
    }
  });
}

var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
$(function() {
  $(document).ajaxError(function(event, jqxhr, settings, thrownError) {
    (jqxhr.status == 401) ? window.location.href = "../index.html": "";
  });
  $(window).load(function() {
    $('#preloader').fadeOut('slow', function() {
      $(this).remove();
    });
  });

  $("body").append('<div id="snackbarsuccs"></div><div id="snackbarerror"></div>');
});



// (localStorage.used_id != 1) ?  ($("li.nav-item").eq(1).hide() && $("li.nav-item").eq(5).hide()) : "";


//success toast fn starts here
function showsuccesstoast() {
  var x = document.getElementById("snackbarsuccs");
  x.className = "show";
  setTimeout(function() {
    x.className = x.className.replace("show", "");
  }, 5000);
}

//failure toast fn starts here
function showerrtoast() {
  var x = document.getElementById("snackbarerror");
  x.className = "show";
  setTimeout(function() {
    x.className = x.className.replace("show", "");
  }, 5000);
}

//failure toast fn starts here
function showiperrtoast() {
  var x = document.getElementById("snackbarerror");
  x.className = "show";
  setTimeout(function() {
    x.className = x.className.replace("show", "");
  }, 3000);
}


//function to show img
function productcom(me) {
  var url = $($(me).html()).find('img').attr('src');
  $('.productview1').empty().append("<br><img src='" + url + "' height='300px' width='300px'><br><br>");
}
//image load for product and employee
function productimg(me) {
  var url = $($(me).html()).find('img').attr('src');
  $('.productview1').empty().append("<br><img src='" + url + "' height='300px' width='300px'><br><br>");
}

function employeeimg(me) {
  var url = $($(me).html()).find('img').attr('src');
  $('.employeeprofilee').empty().append("<br><img src='" + url + "' height='300px' width='300px'><br><br>");
}

function viewproduct(me, id, type) {
  var url = $($(me).html()).find('img').attr('src');
  if (url == '../img/noimage.png') {
    $("#appendproductimg").removeClass("scrollmodalimg");
    $('#appendproductimg').empty().append("<center><br><img src='" + url + "' class='img-responsive h100 mb10'><br><br></center>");
    $('#appendproductimgdelivery').empty().append("<center><br><img src='" + url + "' class='img-responsive h100 mb10'><br><br></center>");

  } else {

    $.ajax({
      url: listproductimg_api + id + '/list_images/',
      type: 'get',
      headers: {
        "content-type": 'application/json',
        "Authorization": "Token " + localStorage.token
      },
      success: function(data) {
        $('#appendproductimg,#appendproductimgdelivery').empty();
        if (type == 0) { //if starts here
          if (data.pickup_image.length == 1) {
            $("#appendproductimg").removeClass("scrollmodalimg");
            $('#appendproductimg').append(`<center><br><img src='${data.pickup_image[0].image}' class='img-responsive h100 mb10'><br><br></center>`)

          } else {
            $("#appendproductimg").addClass("scrollmodalimg");

            for (var i = 0; i < data.pickup_image.length; i++) {
              $('#appendproductimg').append(`<div class="col-md-6"><img src='${data.pickup_image[i].image}' class='img-responsive h100 mb10'></div>`)
            }
          }
        } //if for type ends here
        else {
          if (data.delivery_image.length == 1) {
            $('#appendproductimgdelivery').append(`<center><br><img src='${data.delivery_image[0].image}' class='img-responsive h100 mb10'><br><br></center>`)

          } else {
            for (var i = 0; i < data.delivery_image.length; i++) {
              $('#appendproductimgdelivery').append(`<div class="col-md-6"><img src='${data.delivery_image[i].image}' class='img-responsive h100 mb10'></div>`)
            }
          }
        } //else for type ends here
        // }

      },
      error: function(data) {
        for (var key in JSON.parse(data.responseText)) {
          $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
      }
    });
  } //else ends here
}

//change password modal srch_inpt// change password starts here
$('.currentpasswrd').focusout(function() {
  if ($('.currentpasswrd').val() == "") {
    $("#snackbarerror").text("Current password is required");
    showerrtoast();
    event.stopPropagation();
    return;

  }
});
$('.newpasswrd').focusout(function() {
  if ($('.newpasswrd').val() == "") {
    $("#snackbarerror").text("New password is required");
    showerrtoast();
    event.stopPropagation();
    return;

  }
});
$('.renewpasswrd').focusout(function() {
  if ($('.renewpasswrd').val() == "") {
    $("#snackbarerror").text("Re-enter new password is required");
    showerrtoast();
    event.stopPropagation();
    return;

  }
});

function changepassword() {
  if ($('.currentpasswrd').val() == "") {
    $("#snackbarerror").text("Current password is required");
    showerrtoast();
    event.stopPropagation();
    return;

  } else if ($('.newpasswrd').val() == "") {
    $("#snackbarerror").text("New password is required");
    showerrtoast();
    event.stopPropagation();
    return;

  } else if ($('.renewpasswrd').val() == "") {
    $("#snackbarerror").text("Re-enter new password is required");
    showerrtoast();
    event.stopPropagation();
    return;

  } else if ($('.newpasswrd').val() != $('.renewpasswrd').val()) {
    $("#snackbarerror").text("New password and re-enter new password must be same");
    showerrtoast();
    event.stopPropagation();
    return;

  } else if ($('.currentpasswrd').val() == $('.renewpasswrd').val()) {
    $("#snackbarerror").text("Current password and re-enter new password must not be same");
    showerrtoast();
    event.stopPropagation();
    return;

  } else {
    changepasswordfunc();
  }
}

function changepasswordfunc() {

  $(".changepassBtn").html('Change <img src="../img/loader.gif">');
  $(".changepassBtn").attr("disabled", true);

  var postdata = JSON.stringify({
    "old_password": $(".currentpasswrd").val(),
    "new_password": $(".newpasswrd").val()
  });

  $.ajax({
    url: changepasswordforadmin_api,
    type: 'post',
    data: postdata,
    headers: {
      "content-type": 'application/json'
    },
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Authorization", "Token " + localStorage.token)
    },
    success: function(data) {

      $(".changepassBtn").html('Change');
      $(".changepassBtn").attr("disabled", false);
      $(".closechangepass").click();

    },
    error: function(data) {
      $(".changepassBtn").html('Change');
      $(".changepassBtn").attr("disabled", false);

      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
      

    }
  }).done(function(dataJson) {
    $(".currentpasswrd,.newpasswrd,.renewpasswrd").val("");
    $("#snackbarsuccs").text("Password Changed Sucessfully!");
    showsuccesstoast();
    // var pageURL = $(location).attr("href");
    // window.location.href = pageURL;
  });

} //change password fn ends here

//search function start here
function searchbtnfun(type) {
  // $('#urlloader').empty();
  // $('#urlloader').append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
  if (type != 0) {
    $(".findalldr").show();
    $(".gobtnall").attr("disabled", true);
    $('.datepickerorder').val("");
    $("#search_query").val("");
  }
  var value = $("li.tab_clr.active").find("a").data("attr_val");
  $('#urlloader' + (value + 1)).empty().append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);

  switch (value) {
    case 0:
      loadunconformed_orders(0);
      break;
    case 1:
      listorders(0);
      break;
    case 2:
      listpendingorders(0);
      break;
    case 3:
      listcompletedorders(0);
      break;
    case 4:
      listcancelledorders(0);
      break;
    case 5:
      requestcancelled(0);
      break;
  }

}

$("#search_query").keyup(function() {
  searchbtnfun(0);
})
//logout function starts here
//logout fn starts here
function logout() {

  $.ajax({
    url: logout_api,
    type: 'post',
    headers: {
      "content-type": 'application/json'
    },
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Authorization", "Token " + localStorage.token)
    },
    success: function(data) {
      sessionStorage.clear();
      localStorage.clear();

    },
    error: function(data) {
      sessionStorage.clear();
      localStorage.clear();
      window.location.replace("../index.html");
    }
  }).done(function(dataJson) {
    window.location.replace("../index.html");
  });
} //logout fn starts here

var notifyInterval;

// //notification count
if (localStorage.token) {
  var notifyInterval = setInterval(function() {
    loadheader();
  }, 1000 * 60);
}



//function for editin starts here
function contentedit(me){
  if($(me).hasClass("fa-pencil-square-o")){
$(me).prev().attr({"contenteditable":"true"});
$(me).prev().addClass("editable-true");
$(me).removeClass("fa-pencil-square-o").addClass("fa-check-square-o");
}else if ($(me).hasClass("fa-check-square-o")){
$(me).prev().attr({"contenteditable":"false"});
$(me).prev().removeClass("editable-true");
$(me).addClass("fa-pencil-square-o").removeClass("fa-check-square-o");
}
}




//function starts here for checkbox

function ordertype(type){
  address_details = [];
$("#extraaddr").empty();
  if(type == 1){
    $("#dynamic").empty().append(`<div class="row topic-dashline">
                                <p class="heading_attr p0-1">Delivery Details :</p>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm delivery-name-len" id="deliveryname" maxlength="50" placeholder="Delivery Name*">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="15" id="deliverypn" placeholder="Delivery Number*">
                                    </div>
                                </div>
                                <div class="col-md-6" id="delmul">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm autocompleteip" id="dropaddr" maxlength="253" placeholder="Drop-Address*" style="height: 30px;">
                                    </div>
                                </div>
                            </div>
<div class="row topic-dashline p-2">
                            <div class="row"><div class="mb-25"><p class="heading_attr p0-1 dis-inline">Pickup Details :</p> <button type="button" onclick="triggermodal()" class="btn btn-circle mbtn mdlfsubmitbtn etrclkpedit viewdetails_btn"><i class="fa fa-info aria-hidden="true"></i>&nbsp;&nbsp;view pickup details<sup class="addsup"></sup></button></div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control json-name input-sm pickup-name-len" id="pickupname" maxlength="50" placeholder="Pickup Name*">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control json-number input-sm" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="15" id="pickuppn" placeholder="Pickup Number*">
                                    </div>
                                </div>
                                <div class="col-md-6" id="pickmul">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm json-addr autocompleteip" id="pickupaddr" maxlength="253" placeholder="Pickup-Address*" style="height: 30px;">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row" id="single_order">
                                <p class="heading_attr p0-1">Product Details :</p>
                                <div class="replicatediv">
                                    <div class="col-lg-10">
                                        <div class="form-group form-md-line-input  has-info is-empty ptop0 mb20">
                                            <select class="form-control ipclkaddemp edited roleclr addcatgyreplica" id="addcatgy">
                                                <option value="0">Choose Your Category *</option>${categories}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn btn-circle btn-icon-only fsearchrbtn addanother_prod" onclick="replicate(this);">
                                            <i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="addsub_replicates">
                                        <div class="subreplicate">
                                            <div class="col-md-4">
                                                <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                                    <input type="text" class="form-control input-sm prod_name" maxlength="30" placeholder="Product Name*">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                                    <input type="text" class="form-control input-sm prod_count" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="2" placeholder="Product count*">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                 <div class="btn-group btn-group-vertical quantity" data-toggle="buttons">
                                        ${quantities}</div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <a class="btn btn-circle btn-icon-only fsearchrbtn subaddprod" onclick="replicatesub(this);">
                                                    <i class="fa fa-plus" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="show_replica"></div>

                                <div><button type="button" onclick="address()" class="btn btn-circle mbtn mdlfsubmitbtn etrclkpedit width100">Add Pickup Address</button></div>
                            </div>
                            `);
  }else if(type == 2){
    $("#dynamic").empty().append(`<div class="row topic-dashline">
                                <p class="heading_attr p0-1">Pickup Details :</p>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm pickup-name-len" id="pickupname" maxlength="50" placeholder="Pickup Name*">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control json-number input-sm" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="15" id="pickuppn" placeholder="Pickup Number*">
                                    </div>
                                </div>
                                <div class="col-md-6" id="pickmul">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm autocompleteip" id="pickupaddr" maxlength="253" placeholder="Pickup-Address*" style="height: 30px;">
                                    </div>
                                </div>
                            </div>
                            <div class="row topic-dashline p-2">
                            <div class="row">
                            <div class="mb-25"><p class="heading_attr p0-1 dis-inline">Delivery Details :</p> <button type="button" onclick="triggermodal()" class="btn btn-circle mbtn mdlfsubmitbtn etrclkpedit viewdetails_btn"><i class="fa fa-info aria-hidden="true"></i>&nbsp;&nbsp;view delivery details<sup class="addsup"></sup></button></div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control json-name input-sm delivery-name-len" id="deliveryname" maxlength="50" placeholder="Delivery Name*">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="15" id="deliverypn" placeholder="Delivery Number*">
                                    </div>
                                </div>
                                <div class="col-md-6" id="delmul">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm json-addr autocompleteip" id="dropaddr" maxlength="253" placeholder="Drop-Address*" style="height: 30px;">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row" id="single_order">
                                <p class="heading_attr p0-1">Product Details :</p>
                                <div class="replicatediv">
                                    <div class="col-lg-10">
                                        <div class="form-group form-md-line-input  has-info is-empty ptop0 mb20">
                                            <select class="form-control ipclkaddemp edited roleclr addcatgyreplica" id="addcatgy">
                                                <option value="0">Choose Your Category *</option>${categories}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn btn-circle btn-icon-only fsearchrbtn addanother_prod" onclick="replicate(this);">
                                            <i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="addsub_replicates">
                                        <div class="subreplicate">
                                            <div class="col-md-4">
                                                <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                                    <input type="text" class="form-control input-sm prod_name" maxlength="30" placeholder="Product Name*">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                                    <input type="text" class="form-control input-sm prod_count" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="2" placeholder="Product count*">
                                                </div>
                                            </div>
                                           <div class="col-md-4">
                                                 <div class="btn-group btn-group-vertical quantity" data-toggle="buttons">
                                        ${quantities}
                                    </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <a class="btn btn-circle btn-icon-only fsearchrbtn subaddprod" onclick="replicatesub(this);">
                                                    <i class="fa fa-plus" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="show_replica"></div>
                                 <div><button type="button" onclick="address()" class="btn btn-circle mbtn mdlfsubmitbtn etrclkpedit width100">Add Delivery Address</button></div>
                                
                            </div>
                           `);
  }else if(type == 0){
     $("#dynamic").empty().append(`<div class="row topic-dashline">
                                <p class="heading_attr p0-1">Pickup Details :</p>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm pickup-name-len" id="pickupname" maxlength="50" placeholder="Pickup Name*">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="15" id="pickuppn" placeholder="Pickup Number*">
                                    </div>
                                </div>
                                <div class="col-md-6" id="pickmul">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm autocompleteip" id="pickupaddr" maxlength="253" placeholder="Pickup-Address*" style="height: 30px;">
                                    </div>
                                </div>
                            </div>
                            <div class="row topic-dashline">
                                <p class="heading_attr p0-1">Delivery Details :</p>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm delivery-name-len" id="deliveryname" maxlength="50" placeholder="Delivery Name*">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="15" id="deliverypn" placeholder="Delivery Number*">
                                    </div>
                                </div>
                                <div class="col-md-6" id="delmul">
                                    <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                        <input type="text" class="form-control input-sm autocompleteip" id="dropaddr" maxlength="253" placeholder="Drop-Address*" style="height: 30px;">
                                    </div>
                                </div>
                            </div>
                            <div class="row topic-dashline" id="single_order">
                                <p class="heading_attr p0-1">Product Details :</p>
                                <div class="replicatediv">
                                    <div class="col-lg-10">
                                        <div class="form-group form-md-line-input  has-info is-empty ptop0 mb20">
                                            <select class="form-control ipclkaddemp edited roleclr addcatgyreplica" id="addcatgy">
                                                <option value="0">Choose Your Category *</option>${categories}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn btn-circle btn-icon-only fsearchrbtn addanother_prod" onclick="replicate(this);">
                                            <i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="addsub_replicates">
                                        <div class="subreplicate">
                                            <div class="col-md-4">
                                                <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                                    <input type="text" class="form-control input-sm prod_name" maxlength="30" placeholder="Product Name*">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group form-md-line-input  has-info ptop0 mb20">
                                                    <input type="text" class="form-control input-sm prod_count" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="2" placeholder="Product count*">
                                                </div>
                                            </div>
                                           <div class="col-md-4">
                                                 <div class="btn-group btn-group-vertical quantity" data-toggle="buttons">
                                        ${quantities}
                                    </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <a class="btn btn-circle btn-icon-only fsearchrbtn subaddprod" onclick="replicatesub(this);">
                                                    <i class="fa fa-plus" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="show_replica"></div>
                            </div>`);
  }
   initialize();
}




//remove addr starts here
function removeaddr(me){
  $(me).parent().prev().prev().prev().remove();
  $(me).parent().prev().prev().remove();
  $(me).parent().next().remove();
  $(me).parent().prev().remove();
  $(me).parent().remove();
}

// var bodyText = $("span.pickuploc").val();
// var body = $("<textarea> </textarea>");
// body.text(bodyText);
// $("span.pickuploc").replaceWith(body);


