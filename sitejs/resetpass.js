$(function() {

    $(".rsperr").hide();

    // $('#password,#retype_password').focusin(function(event) {
    //     if ($('.rsperr').is(':visible'))
    //         $('.rsperr').hide();
    // });

    $(".ipclkrsp").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".etrclkrsp").click();
        }
    });

    //validation fn starts here
    $("#password").keyup(function() {
        if ($("#password").val() == "") {
            $("#snackbarerror").text("Password is Required");
            showerrtoast();
        }
    });
    $("#retype_password").keyup(function() {
        if ($("#retype_password").val() == "") {
            $("#snackbarerror").text("Re-enter password is Required");
            showerrtoast();
        }

    });

    //get query string fn starts here
    function getQueryStrings() {
        var assoc = {};
        var decode = function(s) {
            return decodeURIComponent(s.replace(/\+/g, " "));
        };
        var queryString = location.search.substring(1);
        var keyValues = queryString.split('&');
        for (var i in keyValues) {
            var key = keyValues[i].split('=');
            if (key.length > 1) {
                assoc[decode(key[0])] = decode(key[1]);
            }
        }
        return assoc;
    }
    var qs = getQueryStrings();
    var uid = qs["id"];
    var token = qs["token"];
    sessionStorage.resettoken = token;
    sessionStorage.resetuser_id = uid;


}); //initila fn endss here

// reset password fn starts here
function rspcheck() {

    $(".rsperr").hide();

    if ($('#password').val().trim() == '') {
        $("#password").css({
            "border-bottom": "3px solid red"
        });
        $("#snackbarerror").text("Password is Required");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#password').val().length < 6) {
        $("#password").css({
            "border-bottom": "3px solid red"
        });
        $("#snackbarerror").text("Min 6 Characters is Required for Password!");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#retype_password').val().trim() == '') {
        $("#retype_password").css({
            "border-bottom": "3px solid red"
        });
        $("#snackbarerror").text("Re-enter password is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#retype_password').val().length < 6) {
        $("#retype_password").css({
            "border-bottom": "3px solid red"
        });
        $("#snackbarerror").text("Min 6 Characters is Required for Re-Enter Password!");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#retype_password').val() != $('#password').val()) {
        $("#snackbarerror").text("Passwords Do Not Match!");
        showerrtoast();
        event.stopPropagation();
        return;
    }


    $(".rspbtn").html('<img src="img/loader.gif">');
    $(".rspbtn").attr("disabled", true);

    var uniqueclient = Math.random() * 10000000000000000;

    var postData = JSON.stringify({
        "password": $('#password').val(),
        "uid": sessionStorage.getItem('resetuser_id'),
        "token": sessionStorage.getItem('resettoken'),
        "client": uniqueclient,
        "role":1
    });

    $.ajax({
        url: resetpassword_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {
            $(".rspbtn").html('Submit');
            $(".rspbtn").attr("disabled", false);
        },
        error: function(data) {
            $(".rspbtn").html('Submit');
            $(".rspbtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();
        }
    }).done(function(dataJson) {

        $("#snackbarsuccs").text("Your Password Has Been Set Successfully!");
        showsuccesstoast();

        localStorage.logincred = JSON.stringify(dataJson);
        localStorage.wufname = dataJson.first_name;
        localStorage.wulname = dataJson.last_name;
        localStorage.wutkn = dataJson.token;
        localStorage.wuuser_id = dataJson.id;
        localStorage.wuemail = dataJson.email;
        localStorage.wuphone = dataJson.username;


        setTimeout(function() {
            $(".rsperr").text("").hide();
            window.location.href = "index.html";
        }, 2000);

    });

} // reset password fn ends here
