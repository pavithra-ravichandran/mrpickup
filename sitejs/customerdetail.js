//initial function starts here
$(function() {
  loadcustomerdetails(0);
});

function loadcustomerdetails(type) {
  var postData = JSON.stringify({
    "end_date": $("#date_picker1").val(),
    "start_date": $("#date_picker").val()
  });

  if (type == 0) {
    url = customerhistory_api + sessionStorage.customer_id + '/list_orders/';
  } else {
    url = type;
  }

  $.ajax({
    url: url,
    type: "POST",
    data: postData,
    headers: {
      "Authorization": "Token " + localStorage.token,
      "content-type": 'application/json'
    }
  }).done(function(data) {
    $(".findalldr").hide();
    $(".findbtnall").attr("disabled", false);
    $(".listcustomer_details,.imgemptyappend").empty();
    //     "results": {
    //        "id": 1,
    //             "rating": 0,
    //             "distance": 0,
    //             "amount": 80,
    //             "pickup_type": true,
    //             "delivery_name": "delivery boy",
    //             "status": "Cancel"
    //        }
    (data.customer.userprofile.profile_pic != null) ? $(".customerimage").attr("src", data.customer.userprofile.profile_pic): "";
    $(".empnameappd").text(data.customer.first_name);
    (data.customer.username != null) ? $(".emailnameclass").text(data.customer.username): "Not mentioned";

    if (data.results.length != 0) {
      var page = url.split("=");
      for (var i = 0; i < data.results.length; i++) {
        var odrdate = data.results[i].created_on.split('T')
        var content = odrdate[0].split('-');
        var orderdate = content[2] + " " + month[content[1] - 1] + " " + content[0];
        $(".listcustomer_details").append(`  <tr>
        <td>${i+1 * parseInt((page[1] == undefined) ? "1" : page[1] )}</td>
        <td>${data.results[i].order_id}</td>
        <td>${orderdate}</td>
        <td>${data.results[i].distance} kms</td>
        <td>Rs.${data.results[i].amount}</td>
        <td>
            <center><img src="../img/buy.svg" class="w20"></center>
        </td>
        <td>${(data.results[i].delivery_boy != null)? "<a onclick='gotodetailedpage("+data.results[i].delivery_boy.id+")'>"+ data.results[i].delivery_boy.name+"</a>":
        "Not Mentioned" }</td>
        <td>${data.results[i].rating}</td>
        <td>${data.results[i].status}</td>
    </tr>`)
      }
    } else {
      $(".custthead").hide();
      $(".imgemptyappend").empty().append('<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>');
    }
    if (data.next_url != null) {
      var next1 = (data.next_url).split('=');
      var val = data.count / data.page_size;
      if (val % 1 === 0) {
        //if number is integer
        var val = val;
      } else {
        var val = parseInt(val) + 1;
      }
      var obj = $('#pagination').twbsPagination({
        totalPages: val,
        visiblePages: 5,
        onPageClick: function(event, page) {
          console.info(page);
          loadcustomerdetails(next1[0] + "=" + (page));
        }
      });
    }
  }).fail(function(data) {
    for (var key in JSON.parse(data.responseText)) {
      $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
    }
    showerrtoast();
  })
}

//find function starts here
function find() {
  $('#urlloader').empty();
  $('#urlloader').append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
  loadcustomerdetails(0);

}

//find all function starts here
function findall() {
  $('#urlloader').empty();
  $('#urlloader').append(`<ul class="list-inline pagination floatright" id="pagination"></ul>`);
  $("#date_picker1,#date_picker").val("");
  $(".findalldr").show();
  $(".findbtnall").attr("disabled", true);
  loadcustomerdetails(0);
}



//detail page nav starts here
function gotodetailedpage(id) {
  sessionStorage.employee_id = id;
  window.location.href = "employeedetail.html";
}
