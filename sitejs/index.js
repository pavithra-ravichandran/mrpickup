$(function() {


    //enter click fn signup/login starts here
    $(".ipclklgn").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".etrclklgn").click();
        }
    });

    $('#loginemailid').focusout(function() {
        var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
        var str = $('#loginemailid').val();
        if (numberRegex.test(str)) {
            var phone = $('#loginemailid').val();
            var phoneNum = phone.replace(/[^\d]/g, '');
            if (phoneNum.length < 10 || phoneNum.length > 11) {
                emailcheck = 0;
            }
        } else {
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^â€‹_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._â€‹~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            if (!pattern.test($(this).val())) {
                // $("#loginemail").val('');
                var x = $('#loginemailid').val();
                var atpos = x.indexOf("@");
                var dotpos = x.lastIndexOf(".");
                if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                    emailcheck = 1;
                    $('#loginemailid').css({ "border-bottom": "3px solid #e74c3c" });
                    event.stopPropagation();
                    return;
                }
            }
        }
    });

    $("#loginemailid").keyup(function() {
        if ($("#loginemailid").val() == "")
            $("#loginemailid").css({
                "border-bottom": "3px solid red"
            });
        else
            $("#loginemailid").css({
                "border": ""
            });
    });

    $("#loginpasswrd").keyup(function() {
        if ($("#loginpasswrd").val() == "")
            $("#loginpasswrd").css({
                "border-bottom": "3px solid #e74c3c"
            });
        else
            $("#loginpasswrd").css({
                "border": ""
            });
    });

});

//user login fn starts here
function userlogin() {

    if ($('#loginemailid').val().trim() == '') {
        $("#loginemailid").css({
            "border-bottom": "3px solid #e74c3c"
        });
        event.stopPropagation();
        return;
    }

    var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
    var str = $('#loginemailid').val();
    if (numberRegex.test(str)) {
        var phone = $('#loginemailid').val();
        var phoneNum = phone.replace(/[^\d]/g, '');
        if (phoneNum.length < 10 || phoneNum.length > 11) {
            $('#loginemailid').css({ "border-bottom": "3px solid #e74c3c" });
            event.stopPropagation();
            return;
        }
    } else {
        var x = $('#loginemailid').val();
        var atpos = x.indexOf("@");
        var dotpos = x.lastIndexOf(".");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {

            $('#loginemailid').css({ "border-bottom": "3px solid #e74c3c" });
            event.stopPropagation();
            return;
        }

    }


    if ($('#loginpasswrd').val().trim() == '') {
        $("#loginpasswrd").css({
            "border-bottom": "3px solid #e74c3c"
        });
        event.stopPropagation();
        return;
    }

    $(".lgnBtn").html('<img src="img/loader.gif">');
    $(".lgnBtn").attr("disabled", true);

    var uniqueclient = Math.random() * 10000000000000000;

    var postData = JSON.stringify({

        "username": $('#loginemailid').val(),
        "password": $('#loginpasswrd').val(),
        "client": uniqueclient

    });

    $.ajax({
        url: login_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {

            $(".lgnBtn").html('Login');
            $(".lgnBtn").attr("disabled", false);

        },
        error: function(data) {

            $(".lgnBtn").html('Login');
            $(".lgnBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {


        $("#snackbarsuccs").text("Login Success!");
        showsuccesstoast();

        //  {
        //   "id": 1,
        //   "first_name": "Krishna",
        //   "last_name": "Madhavan",
        //   "username": "9094483144",
        //   "email": "gopi@billiontags.com",
        //   "token": "4af4dff8f01e4f2e8a5dfffce6086cb286db917f",
        //   "userprofile": {
        //     "profile_pic": "http://192.168.1.8:8000/media/Disha-Patani.jpg"
        //   }
        // }

        localStorage.logincred = JSON.stringify(dataJson);
        localStorage.wufname = dataJson.first_name;
        localStorage.wulname = dataJson.last_name;
        localStorage.wutkn = dataJson.token;
        localStorage.wuuser_id = dataJson.id;
        localStorage.wuemail = dataJson.email;
        localStorage.wuphone = dataJson.username;
        localStorage.wupropic = dataJson.userprofile.profile_pic;

        window.location.href = "admin_4/dashboard.html";


    }); //done fn ends here

} //login fn ends here
