function listcancelledorders(type) {
  sessionStorage.order_type = 4;
  sessionStorage.order_page = type;


  if(type == 0){
    var url = listorder_api;
  }else{
    var url = type;
  }

  var postData = JSON.stringify ({
    "status": 5,
    "start_date": $("#date_picker").val(),
    "end_date": $("#date_picker1").val(),
    "search": $("#search_query").val()
  });

    $.ajax({
        url: url,
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.token
        },
        success: function(data) {
            $('.cancelledlist').empty();
            $(".findalldr").hide();
            $(".gobtnall").attr("disabled",false);
            if (data.results.length == 0) {
                $(".cancelledclass").hide();
                $(".cancelledremove").empty().prepend('<center><img id="theImg" src="../img/ndf.png" height="250px" width="250px"/>');

            } else {
              $(".cancelledclass").show();
                for (var i = 0; i < data.results.length; i++) {

                    var picktype = (data.results[i].pickup_type) ? '../img/pick.svg' : '../img/buy.svg';
                    if (data.results[i].pickup_image == null  || data.results[i].pickup_image.length == 0) {
                        var image = '../img/noimage.png';
                    } else {
                        var image = data.results[i].pickup_image[0].image;
                    }

                    var created_on = data.results[i].created_on;
                    var newdate = created_on.split('T');
                    var now = newdate[0].split('-');
                    var timeval = Convert_time(newdate[1].slice(0, 8));
                    var datenew = now[2] + '/' + now[1] + '/' + now[0];

                    $(".cancelledlist").append(`<tr><td>${(i + 1)}</td><td>${data.results[i].order_id}</td>
                    <td onclick="viewproduct(this,${data.results[i].id},0)"><center><img src="${image}" class="img-responsive classborderedimg  tab_img" href="#viewfullimg" data-toggle="modal"></center></td><td>${data.results[i].pickup_name}</td><td class="whitenowrap">${datenew} ${timeval}</td><td>${data.results[i].pickup_loc}</td><td>${data.results[i].del_loc}</td><td><img src=${picktype} class="iconw50"></td></tr>`);
                }
            }
            if ((data.next_url != null) || (data.prev_url != null)) {
              $("#urlloader").show();

              var next1 = (data.next_url).split('=');
              var val = data.count / data.page_size;
              if (val % 1 === 0) {
                //if number is integer
                var val = val;
              } else {
                var val = parseInt(val) + 1;
              }
              var obj = $('#pagination').twbsPagination({
                totalPages: val,
                visiblePages: 5,
                onPageClick: function(event, page) {
                  console.info(page);
                  listcancelledorders(next1[0] + "=" + (page));
                }
              });
            }else{
              $("#urlloader").hide();
            }
        },
        error: function(data) {
          $(".findalldr").hide();
          $(".gobtnall").attr("disabled",false);
          for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
          }
          showerrtoast();
        }
    });
}
