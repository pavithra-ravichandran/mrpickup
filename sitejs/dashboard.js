$.ajax({

    url: dashboardcountvalue_api,
    type: 'GET',
    headers: {
        "content-type": 'application/json',
        "Authorization": "Token " + localStorage.token
    },
    success: function(data) {

        var activeemployee = (data.active_user_count == 0) ? 0 : data.active_user_count;
        var orderplaced = (data.placed_order == 0) ? 0 : data.placed_order;
        var orderpending = (data.pending_order == 0) ? 0 : data.pending_order;
        var ordercompleted = (data.completed_order == 0) ? 0 : data.completed_order;
        var ordercancelled = (data.cancel_order == 0) ? 0 : data.cancel_order;
        var offerdetails = (data.offer_count == 0) ? 0 : data.offer_count;
        var totalorders = (data.total_order == 0) ? 0 : data.total_order;
        var totalusers = (data.total_use== 0) ? 0: data.total_use;
        //precentage calculation
        var activeuser_perc = (activeemployee / totalusers) *100 ;
        var placedorder_prec = (orderplaced / totalorders) *100 ;
        var pendingorder_prec = (orderpending / totalorders) *100 ;
        var completedorder_prec = (ordercompleted / totalorders) *100 ;
        var cancelledorder_prec = (ordercancelled / totalorders) *100 ;

        $(".activeprec").text(parseInt(activeuser_perc) + " %");
        $(".placedprec").text(parseInt(placedorder_prec) + " %");
        $(".pendindprec").text(parseInt(pendingorder_prec) + " %");
        $(".completedprec").text(parseInt(completedorder_prec) + " %");
        $(".cancelledprec").text(parseInt(cancelledorder_prec) + " %");
        $(".offersprec").text(parseInt(offerdetails) + " %");

        $(".progress-bar-success").eq(0).css({
            "width": activeuser_perc+"%"
        });
        $(".progress-bar-success").eq(1).css({
            "width": placedorder_prec+"%"
        });
        $(".progress-bar-success").eq(2).css({
            "width": pendingorder_prec+"%"
        });
        $(".progress-bar-success").eq(3).css({
            "width": completedorder_prec+"%"
        });
        $(".progress-bar-success").eq(4).css({
            "width": cancelledorder_prec+"%"
        });
        $(".progress-bar-success").eq(5).css({
            "width": offerdetails+"%"
        });


        $('.employeeactive').append('<span data-counter="counterup" data-value="">'+parseInt(data.active_user_count)+'</span>');
        $('.ordersplaced').append('<span data-counter="counterup" data-value="">'+parseInt(data.placed_order)+'</span>');
        $('.orderspending').append('<span data-counter="counterup" data-value="">'+parseInt(data.pending_order)+'</span>');
        $('.orderscompleted').append('<span data-counter="counterup" data-value="">'+parseInt(data.completed_order)+'</span>');
        $('.ordercancelled').append('<span data-counter="counterup" data-value="">'+parseInt (data.cancel_order) +'</span>');
        $('.orderoffers').append('<span data-counter="counterup" data-value="">'+parseInt (data.offer_count) +'</span>');

    },
    error: function(data) {
        console.log('error occured in dashboard loading...');
    }
});
