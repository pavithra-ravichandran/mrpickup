$(function() { //initial fn starts here
$(".errormsg").hide();
    $(".ipclkfp").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".etrclkfp").click();
        }
    });

    //validation for frgt password starts here
    $('#useremailfp').focusout(function() {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^â€‹_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._â€‹~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        if (!pattern.test($(this).val())) {
            var x = $('#useremailfp').val();
            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                $("#useremailfp").css({
                    "border-bottom": "3px solid red"
                });
                event.stopPropagation();
                return;
            }
        }
    });
    $("#useremailfp").keyup(function() {
        if ($("#useremailfp").val() == "")
            $("#useremailfp").css({
                "border-bottom": "3px solid red"
            });
        else
            $("#useremailfp").css({
                "border": ""
            });
    });

});

//frgtpassword nrml fn starts here
function userfp() {
$(".errormsg").hide();
    if ($('#useremailfp').val().trim() == '') {
        $("#useremailfp").css({
            "border-bottom": "3px solid red"
        });
         // $(".errormsg").show();
        event.stopPropagation();

        return;
    }

    var email = $('#useremailfp').val();
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
        $("#useremailfp").css({
            "border-bottom": "3px solid red"
        });
          $(".errormsg").show();
        event.stopPropagation();
        return;
    }

    $(".fpBtn").html('<img src="img/loader.gif">');
    $(".fpBtn").attr("disabled", true);

    var uniqueclient = Math.random() * 10000000000000000;

    var postData = JSON.stringify({
        "email": $('#useremailfp').val(),
        "role" : 1
    });

    $.ajax({
        url: forgotpwd_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {
            $(".fpBtn").html('Submit');
            $(".fpBtn").attr("disabled", false);
        },
        error: function(data) {

            $(".fpBtn").html('Submit');
            $(".fpBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {

        $("#snackbarsuccs").text("Please Check Your Mail To Reset Password!");
        showsuccesstoast();

        setTimeout(function() {
            window.location.href = "index.html";
        }, 4000);

    }); //done fn ends here

} //frgtpassword fn ends here
