$(function() { //initial fn starts here

    //hide error statement and loader
    $(".lgnerr,.eiderr,.perr").hide();

    //enter click fn starts here
    $(".ipclklgn").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".etrclklgn").click();
        }
    });

    //validation starts here
    $('#loginusername').focusout(function() {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^â€‹_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._â€‹~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        if (!pattern.test($(this).val())) {
            var x = $('#loginusername').val();
            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                $('.eiderr').text("Invalid Email-Id").show();
                event.stopPropagation();
                return;
            }
        }
    });

    $("#loginusername").keyup(function() {
        if ($("#loginusername").val() == "")
            $('.eiderr').text("Email-Id is Required").show();
        else
            $('.eiderr').text("").hide();
    });

    $("#password").keyup(function() {
        if ($("#password").val() == "")
            $('.perr').text("Password is Required").show();
        else
            $('.perr').text("").hide();
    });

    //server errtext hide fn starts here
    $("#loginusername,#password").focusin(function() {
        if ($('.lgnerr').is(':visible')) {
            $('.lgnerr').hide();
        }
    });

}); //initial fn ends here


//login fn starts here
function dologin() {

    $(".lgnerr,.eiderr,.perr").hide();

    if ($('#loginusername').val().trim() == '') {
        $('.eiderr').text("Email-Id is Required").show();
        event.stopPropagation();
        return;
    }

    var email = $('#loginusername').val();
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
        $('.eiderr').text("Invalid Email-Id").show();
        event.stopPropagation();
        return;
    }

    if ($('#password').val().trim() == '') {
        $('.perr').text("Password is Required").show();
        event.stopPropagation();
        return;
    }

    $(".lgnbtn").html('<img src="assets/img/loader.gif">');

    var uniqueclient = Math.random() * 10000000000000000;

    var postData = JSON.stringify({

        "username": $('#loginusername').val(),
        "password": $('#password').val(),
        "client": uniqueclient

    });

    $.ajax({
        url: login_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {
            $(".lgnbtn").html('Submit');
        },
        error: function(data) {

            $(".lgnbtn").html('Submit');
            $(".lgnerr").text(jQuery.parseJSON(data.responseText)['non_field_errors']).show();

            setTimeout(function() {
                $(".lgnerr").text("").hide();
            }, 3000);

        }
    }).done(function(dataJson) {

        $(".lgnerr").text("Login Success !").show();

        setTimeout(function() {

            localStorage.fname = dataJson.first_name;
            localStorage.token = dataJson.userprofile.token;
            localStorage.user_id = dataJson.id;

            window.location.href = '../admin_4/listusers.html';

        }, 1000);

    }); //done fn ends here

} //login fn ends here
